package io.sdlts.common.test.utils;

import com.google.common.collect.ImmutableList;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.model.Address;
import io.sdlts.common.model.Country;
import io.sdlts.common.model.PartyInformation;
import net.corda.core.concurrent.CordaFuture;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import net.corda.core.messaging.FlowHandle;
import net.corda.testing.core.TestIdentity;
import net.corda.testing.driver.DriverDSL;
import net.corda.testing.driver.DriverParameters;
import net.corda.testing.driver.NodeHandle;
import net.corda.testing.driver.NodeParameters;
import net.corda.testing.node.TestCordapp;
import net.corda.testing.node.User;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

import static net.corda.testing.driver.Driver.driver;

public abstract class AbstractPipelineTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPipelineTest.class);
    public static final String BRUSSELS = "Brussels";

    protected String adminUser = "admin";
    protected String testUser = "test";

    protected final TestIdentity sellerNode = new TestIdentity(new CordaX500Name("SELLER", "Leuven", "BE"));
    protected final TestIdentity buyerNode = new TestIdentity(new CordaX500Name("BUYER", "Ghent", "BE"));
    protected final TestIdentity thirdPartyNode = new TestIdentity(new CordaX500Name("Third Party Node", "Antwerpen", "BE"));
    protected final TestIdentity taxAuthorityNode = new TestIdentity(new CordaX500Name("TAXES", "Hasselt", "BE"));
    protected final TestIdentity accountantNode = new TestIdentity(new CordaX500Name("ACCOUNTANT", BRUSSELS, "BE"));
    protected NodeHandle sellerHandle;
    protected NodeHandle buyerHandle;
    protected NodeHandle thirdPartyHandle;
    protected NodeHandle taxAuthorityHandle;
    protected NodeHandle accountantHandle;
    protected Party sellerParty;
    protected Party buyerParty;
    protected Party thirdParty;
    protected Party taxesParty;
    protected Party accountantParty;

    @Test
    public void run() {
        List<String> cordappsToRegister = new ArrayList<>();
        cordappsToRegister.add("com.r3.corda.lib.accounts.contracts");
        cordappsToRegister.add("com.r3.corda.lib.accounts.workflows");

        cordappsToRegister.add("io.sdlts.common.contracts");
        cordappsToRegister.add("io.sdlts.common.workflows");

        cordappsToRegister.add("net.corda.core.contracts");
        cordappsToRegister.add("net.corda.core.flows");

        registerMyCordapps(cordappsToRegister);

        List<TestCordapp> cordapps = new ArrayList<>();

        for (String app : cordappsToRegister) {
            cordapps.add(TestCordapp.findCordapp(app));
        }

        driver(new DriverParameters()
                .withIsDebug(true)
                .withCordappsForAllNodes(cordapps)
                .withStartNodesInProcess(true), this::executePipelineDSL);
    }

    protected abstract void registerMyCordapps(List<String> cordappsToRegister);

    public Object executePipelineDSL(DriverDSL dsl) {
        try {
            List<User> users = new ArrayList<>();
            Set<String> permissions = new HashSet<>();
            permissions.add("StartFlow.io.sdlts.common.workflows.SignupFlow");
            permissions.add("StartFlow.io.sdlts.common.workflows.FindAccountInfoFlow");
            permissions.add("StartFlow.io.sdlts.common.workflows.UpdatePartyInformationFlow");
            permissions.add("StartFlow.io.sdlts.common.workflows.GetPartyInformationFlow");

            registerPermissions(permissions);

            permissions.add("StartFlow.io.sdlts.common.workflows.SuperSampleFlow");

            User user = new User("test", "test", permissions);
            users.add(user);

            List<CordaFuture<NodeHandle>> handleFutures = ImmutableList.of(
                    dsl.startNode(new NodeParameters().withRpcUsers(users).withProvidedName(sellerNode.getName())),
                    dsl.startNode(new NodeParameters().withProvidedName(buyerNode.getName())),
                    dsl.startNode(new NodeParameters().withProvidedName(thirdPartyNode.getName())),
                    dsl.startNode(new NodeParameters().withProvidedName(taxAuthorityNode.getName())),
                    dsl.startNode(new NodeParameters().withProvidedName(accountantNode.getName()))

            );

            sellerHandle = handleFutures.get(0).get();
            buyerHandle = handleFutures.get(1).get();
            thirdPartyHandle = handleFutures.get(2).get();
            taxAuthorityHandle = handleFutures.get(3).get();
            accountantHandle = handleFutures.get(4).get();

            sellerParty = buyerHandle.getRpc().wellKnownPartyFromX500Name(sellerNode.getName());
            buyerParty = buyerHandle.getRpc().wellKnownPartyFromX500Name(buyerNode.getName());
            thirdParty = buyerHandle.getRpc().wellKnownPartyFromX500Name(thirdPartyNode.getName());
            taxesParty = buyerHandle.getRpc().wellKnownPartyFromX500Name(taxAuthorityNode.getName());
            accountantParty = buyerHandle.getRpc().wellKnownPartyFromX500Name(accountantNode.getName());

            LOGGER.info("Starting test...");
            executePipeline(handleFutures);
            LOGGER.info("Test completed.");
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e.getMessage(), e);
        }
        return null;
    }

    protected abstract void registerPermissions(Set<String> permissions);


    protected abstract void executePipeline(List<CordaFuture<NodeHandle>> futureIterable) throws InterruptedException, ExecutionException, NoSuchFieldException, IOException;


    protected <T> CompletableFuture<T> flow(Supplier<FlowHandle<T>> supplier) {
        FlowHandle<T> handle = supplier.get();

        CompletableFuture<T> flowFuture = handle.getReturnValue().toCompletableFuture();
        CompletableFuture<Void> completableFuture = CompletableFuture.allOf(flowFuture);
        while (!completableFuture.isDone()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                e.printStackTrace();
            }
        }
        return flowFuture;
    }

    protected PartyInformation buildBigCompany1() {
        Address address =  new Address.Builder().street("London street 54").zip("3000").city("Aachen").country(Country.of("DE")).build();
        PartyInformation pi = new PartyInformation();
        pi.setName("UAB Big Company");
        pi.setVat("LT 123.456.789");
        pi.setAddress(address);
        return pi;
    }

    protected PartyInformation buildSmallCompany1() {
        Address address =  new Address.Builder().street("London street 54").zip("3000").city("Copenhagen").country(Country.of("DK")).build();
        PartyInformation pi = new PartyInformation();
        pi.setName("Weather company");
        pi.setVat("DK 123.456.789");
        pi.setAddress(address);
        return pi;
    }

    protected PartyInformation buildBigCompany2() {
        Address address =  new Address.Builder().street("Stationstraat 44").zip("5001").city(BRUSSELS).country(Country.of("BE")).build();
        PartyInformation pi = new PartyInformation();
        pi.setName("VOF Big Company 2");
        pi.setVat("BE 123.456.789");
        pi.setAddress(address);
        return pi;
    }

    protected PartyInformation buildThirdParty() {
        Address address =  new Address.Builder().street("Stationstraat 44").zip("5001").city(BRUSSELS).country(Country.of("BE")).build();
        PartyInformation pi = new PartyInformation();
        pi.setName("VBVB My name");
        pi.setVat("BE 123.456.789");
        pi.setAddress(address);
        return pi;
    }

    protected String format(String accountName, Party party) {
        return String.format("ACC=%s, %s", accountName, party.getName().toString());
    }

    @NotNull
    protected UniqueIdentifier findParty(Supplier<Optional<StateAndRef<PartyInformationState>>> supplier) {
        Optional<StateAndRef<PartyInformationState>> partyInformation = supplier.get();

        if (!partyInformation.isPresent()) {
            throw new IllegalStateException("Party information missing");
        }
        return partyInformation.get().getState().getData().getLinearId();
    }

    public class WrongStateError extends Error {

    }
}
