package io.sdlts.common.workflows;

import io.sdlts.common.services.AccountNumberGenerator;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Pattern;

public class AccountGeneratorTest {

    @Test
    public void testDefault() {
        Pattern p = Pattern.compile("^[A-Z]{2}[0-9]{2}-[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{2}[A-Z]{1}[0-9]{1}$");
        String number = new AccountNumberGenerator().generate();
        Assert.assertTrue(p.matcher(number).matches());
    }


    @Test
    public void testValidateName() {
        Pattern p = Pattern.compile("^[A-Za-z0-9 ]*$");
        Assert.assertTrue(p.matcher("Name goes here").matches());
        Assert.assertFalse(p.matcher("Name goes he#re").matches());
        Assert.assertFalse(p.matcher("Name goes he?re").matches());
    }

}
