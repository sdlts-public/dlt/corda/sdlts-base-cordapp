package io.sdlts.common.workflows;

import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.queries.MyPartyInformationQuery;
import net.corda.core.concurrent.CordaFuture;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.testing.driver.NodeHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class SmartAddressAccountTest extends AbstractCommonFlowTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmartAddressAccountTest.class);

    public static final String LINE = "------------------------------------------------------------------------------------------------";

    String accountName = "Fancy account name with address";
    String accountName2 = "Fancy account name with address 2";

    protected void states(NodeHandle nodeHandle, AuthRequest authRequest, String ba) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("---------------- {}  -----------------", nodeHandle.getNodeInfo().getLegalIdentities().get(0).getName());
        }

        List<PartyInformation> result = new MyPartyInformationQuery(AuthRequest.of(authRequest.getToken(), ba), nodeHandle.getRpc()).list();


        for (PartyInformation partyInformation : result) {
            LOGGER.info("Blockchain address: {}, Smart address: {} ", ba, partyInformation);
        }
    }

    @Override
    protected void executeCommonPipeline(List<CordaFuture<NodeHandle>> futureIterable) throws InterruptedException, ExecutionException, NoSuchFieldException {

        String adminToken = createToken(adminUser);

        PartyInformationState accountCreated = flow(() -> sellerHandle.getRpc().startFlowDynamic(SignupFlow.class, AuthRequest.of(adminToken), accountName)).get();
        if (accountCreated == null) {
            throw new IllegalStateException("Account information missing");
        }
        String ba1 = accountCreated.getContent().getBlockchainAddress();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Account[{}] created for account name: {}", ba1, accountName);
        }

        AuthRequest authRequest = AuthRequest.of(adminToken, accountCreated.getContent().getBlockchainAddress());

        UniqueIdentifier partyInformationIdentifier = findParty(() -> new MyPartyInformationQuery(authRequest, sellerHandle.getRpc()).firstState());
        flow(() -> sellerHandle.getRpc().startFlowDynamic(UpdatePartyInformationFlow.class,
                authRequest,
                buildBigCompany2(),
                partyInformationIdentifier
                )).get();
        states(sellerHandle, authRequest, accountCreated.getContent().getBlockchainAddress());

        LOGGER.info(LINE);

        try {
            flow(() -> thirdPartyHandle.getRpc().startFlowDynamic(UpdatePartyInformationFlow.class,AuthRequest.of(adminToken, ba1), buildThirdParty(), partyInformationIdentifier)).get();
            throw new WrongStateError();
        } catch (Exception e) {
            LOGGER.warn("There is no such account on third party host: {}", e.getMessage());
        }

        PartyInformationState accountCreated2 = flow(() -> sellerHandle.getRpc().startFlowDynamic(SignupFlow.class, AuthRequest.of(adminToken), accountName2)).get();
        String ba = accountCreated2.getContent().getBlockchainAddress();
        if (LOGGER.isInfoEnabled()) LOGGER.info("Account created: {}", ba);
        AuthRequest authRequest2 = AuthRequest.of(adminToken, ba);
        UniqueIdentifier partyInformationIdentifier2 = findParty(() -> new MyPartyInformationQuery(authRequest2, sellerHandle.getRpc()).firstState());

        states(sellerHandle, authRequest2, ba);

        try {
            flow(() -> sellerHandle.getRpc().startFlowDynamic(UpdatePartyInformationFlow.class, authRequest, buildBigCompany2(), partyInformationIdentifier2)).get();
            throw new WrongStateError();
        } catch (Exception e) {
            LOGGER.warn("Different user tries to create address on account: {}", e.getMessage());
        }

    }

    @Override
    protected void registerPermissions(Set<String> permissions) {
        //Sub classes should register permissions
        permissions.add("InvokeRpc.vaultQueryByCriteria");
    }
}
