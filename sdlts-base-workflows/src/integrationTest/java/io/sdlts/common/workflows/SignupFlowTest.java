package io.sdlts.common.workflows;

import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.queries.MyAccountsQuery;
import net.corda.core.concurrent.CordaFuture;
import net.corda.testing.driver.NodeHandle;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class SignupFlowTest extends AbstractCommonFlowTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignupFlowTest.class);
    public static final String ACCOUNT_CREATED_FOR_USER = "Account created for user [{}]: {}";

    String accountName = "Fancy account name";
    String accountName2 = "Fancy account name2";

    @Override
    protected void executeCommonPipeline(List<CordaFuture<NodeHandle>> handleFutures) throws InterruptedException, ExecutionException {

        PartyInformationState accountCreated = flow(() -> sellerHandle.getRpc().startFlowDynamic(SignupFlow.class, AuthRequest.of(createToken(adminUser)), accountName)).get();
        if (accountCreated != null)  LOGGER.info(ACCOUNT_CREATED_FOR_USER, adminUser, accountCreated);

        print(sellerHandle, AuthRequest.of(createToken(adminUser)));

        accountCreated = flow(() -> sellerHandle.getRpc().startFlowDynamic(SignupFlow.class, AuthRequest.of(createToken(testUser)), accountName2)).get();
        if (accountCreated != null) LOGGER.info(ACCOUNT_CREATED_FOR_USER, testUser, accountCreated);

        accountCreated = flow(() -> sellerHandle.getRpc().startFlowDynamic(SignupFlow.class, AuthRequest.of(createToken(testUser)), "Another account")).get();
        Assert.assertNotNull(accountCreated);
        LOGGER.info(ACCOUNT_CREATED_FOR_USER, testUser, accountCreated);

        String resolvedBlockchainAddress = accountCreated.getContent().getBlockchainAddress();

        AccountInfo accountInfo = flow(() -> buyerHandle.getRpc().startFlowDynamic(FindAccountInfoFlow.class, BlockchainAddress.parse(resolvedBlockchainAddress))).get();
        Assert.assertNotNull(accountInfo);

        AccountInfo accountInfo1 = flow(() -> buyerHandle.getRpc().startFlowDynamic(FindAccountInfoFlow.class, BlockchainAddress.parse("ACC=Haag Group, O=Proxies, L=Munich, C=DE"))).get();
        Assert.assertNull(accountInfo1);

        print(sellerHandle, AuthRequest.of(createToken(testUser)));
        print(sellerHandle, AuthRequest.of(createToken("fake user")));

    }

    private void print(NodeHandle nodeHandle, AuthRequest authRequest) {
        List<SmartPermissionsState> permissionsStates =  new MyAccountsQuery(authRequest, nodeHandle.getRpc()).list();
        for (SmartPermissionsState permissionsState : permissionsStates) {
            LOGGER.info("[{}] Account address: {}", permissionsState.getUsername(), permissionsState.getPartyAddress());
        }
    }



    @Override
    protected void registerPermissions(Set<String> permissions) {
        permissions.add("InvokeRpc.vaultQueryByCriteria");
    }
}
