package io.sdlts.common.workflows;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import io.sdlts.common.test.utils.AbstractPipelineTest;
import net.corda.core.concurrent.CordaFuture;
import net.corda.testing.driver.NodeHandle;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public abstract class AbstractCommonFlowTest extends AbstractPipelineTest {
    @Override
    protected final void executePipeline(List<CordaFuture<NodeHandle>> futureIterable) throws InterruptedException, ExecutionException, NoSuchFieldException {
        executeCommonPipeline(futureIterable);
    }

    protected abstract void executeCommonPipeline(List<CordaFuture<NodeHandle>> futureIterable) throws InterruptedException, ExecutionException, NoSuchFieldException;

    @Override
    protected void registerMyCordapps(List<String> cordappsToRegister) {
    }

    protected static String createToken(String username) {
        return JWT.create()
                .withAudience(username)
                .withSubject(username)
                .withClaim("email", "email@email.com")
                .withClaim("name", "Demo Demo")
                .sign(Algorithm.HMAC256(UUID.randomUUID().toString()));
    }
}
