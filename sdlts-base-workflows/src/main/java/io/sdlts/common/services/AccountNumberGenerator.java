package io.sdlts.common.services;

import java.util.Random;

public class AccountNumberGenerator {
    private static final Random random = new Random();

    public String generate() {

        String val = String.format("%s%02d-%04d-%04d-%04d-%02d%s",
                generateString(2),
                random.nextInt(99),
                random.nextInt(9999),
                random.nextInt(9999),
                random.nextInt(9999),
                random.nextInt(99),
                generateString(1)
        );
        int checkDigit = getCheckDigit(val);
        return val + checkDigit;
    }

    private int getCheckDigit(String number) {
        int sum = 0;
        for (int i = 0; i < number.length(); i++) {

            int digit;
            String v = number.substring(i, (i + 1));
            if (v.equals("-")) {
                continue;
            }
            if (!isDigit(v)) {
                digit = v.charAt(0);
            } else {
                digit = Integer.parseInt(v);
            }

            if ((i % 2) == 0) {
                digit = digit * 2;
                if (digit > 9) {
                    digit = (digit / 10) + (digit % 10);
                }
            }
            sum += digit;
        }

        int mod = sum % 10;
        return ((mod == 0) ? 0 : 10 - mod);
    }

    private boolean isDigit(String v) {
        try {
            return Integer.parseInt(v) >= 0;
        } catch (Exception e) {
            return false;
        }
    }

    private String generateString(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(find());
        }
        return sb.toString().toUpperCase();
    }

    private char find() {
        char c = randomChar();
        while (c == 'i' || c == 'o') {
            c = randomChar();
        }
        return c;
    }

    private char randomChar() {
        return (char)(random.nextInt(26) + 'a');
    }

}
