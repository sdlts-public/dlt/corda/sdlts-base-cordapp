package io.sdlts.common.services;

import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.model.UserDetails;
import io.sdlts.common.services.tokens.Auth0TokenVerifier;
import io.sdlts.common.services.tokens.LdapTokenVerifier;
import io.sdlts.common.services.tokens.ServiceAccountTokenVerifier;

public class AuthDecoder {
    private final AuthRequest authRequest;

    public AuthDecoder(AuthRequest authRequest) {
        this.authRequest = authRequest;
    }

    public UserDetails decode() {
        UserDetails result;

        switch (authRequest.getAuthType()) {
            case AUTH0:
                result = new Auth0TokenVerifier(authRequest.getToken()).verifyAndGetUser().getRight();
                break;
            case SHIRO:
            case OAUTH2:
            case LDAP:
                result = new LdapTokenVerifier(authRequest.getToken()).verifyAndGetUser();
                break;
            case SERVICE_ACCOUNT:
                result = new ServiceAccountTokenVerifier(authRequest.getToken()).verifyAndGetUser();
                break;
            default:
                throw new IllegalStateException("User decoding is not supported: " + authRequest.getAuthType());
        }
        return result;
    }
}
