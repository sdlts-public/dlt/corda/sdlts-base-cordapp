package io.sdlts.common.services.tokens;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.base.Strings;
import io.sdlts.common.model.UserDetails;
import io.sdlts.common.services.MemoryCache;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.interfaces.RSAPublicKey;
import java.util.concurrent.ExecutionException;

public class Auth0TokenVerifier implements TokenVerifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(Auth0TokenVerifier.class);

    private final String token;

    public Auth0TokenVerifier(String idToken) {
        this.token = idToken;
    }

    public Pair<DecodedJWT, UserDetails> verifyAndGetUser() {
        String domainUrl = System.getenv("SDLTS_AUTH0_DOMAIN");

        if (Strings.isNullOrEmpty(domainUrl)) {
            throw new IllegalStateException("SDLTS_AUTH0_DOMAIN environment variable is missing. Please, configure node env variable.");
        }
        LOGGER.trace("Flow auth0 domain resolved: {}", domainUrl);

        JwkProvider provider = new UrlJwkProvider(domainUrl);
        try {
            return retrieve(provider);
        } catch (InvalidClaimException ex) {
            sleep();
            return retrieve(provider);
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @NotNull
    private Pair<DecodedJWT, UserDetails> retrieve(JwkProvider provider) {
        try {
            DecodedJWT jwt = JWT.decode(token);

            Jwk jwk = provider.get(jwt.getKeyId());

            Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);

            JWTVerifier verifier = JWT.require(algorithm).ignoreIssuedAt().build();
            if (verifier == null) {
                throw new IllegalStateException("Verifier was not created");
            }
            DecodedJWT verifiedJWT = verifier.verify(token);

            UserDetails userDetails = MemoryCache.getLoadingCache().get(token);
            return Pair.of(verifiedJWT, userDetails);
        } catch (JwkException | ExecutionException e) {
            LOGGER.error("Error while retrieve user details", e);
        }
        LOGGER.warn("Could not verity user by token!");
        return Pair.of(null, defaultUserDetails());
    }
}
