package io.sdlts.common.services;

import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.UtilitiesKt;
import kotlin.jvm.internal.Intrinsics;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.identity.Party;
import net.corda.core.node.AppServiceHub;
import net.corda.core.node.services.CordaService;
import net.corda.core.node.services.VaultQueryException;
import net.corda.core.node.services.VaultService;
import net.corda.core.node.services.vault.PageSpecification;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.Sort;
import net.corda.core.serialization.SingletonSerializeAsToken;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@CordaService
public class AccountInfoService extends SingletonSerializeAsToken {
    private final AppServiceHub serviceHub;

    public AccountInfoService(AppServiceHub serviceHub) {
        this.serviceHub = serviceHub;
    }

    public boolean hasAccounts() {
        Party host = serviceHub.getMyInfo().getLegalIdentities().get(0);

        Intrinsics.checkParameterIsNotNull(host, "host");
        VaultService vaultService = serviceHub.getVaultService();
        QueryCriteria queryCriteria = UtilitiesKt.getAccountBaseCriteria().and(UtilitiesKt.accountHostCriteria(host));
        try {
            List<StateAndRef<AccountInfo>> accounts = vaultService._queryBy(queryCriteria, new PageSpecification(1, 2), new Sort(Collections.emptySet()), AccountInfo.class).getStates();
            return ! accounts.isEmpty();
        } catch (VaultQueryException e) {
            e.printStackTrace();
        }

        return true;
    }

    public Optional<AccountInfo> getAccount(String name) {
        Party host = serviceHub.getMyInfo().getLegalIdentities().get(0);

        Intrinsics.checkParameterIsNotNull(host, "host");
        VaultService vaultService = serviceHub.getVaultService();
        QueryCriteria queryCriteria = UtilitiesKt.getAccountBaseCriteria().and(UtilitiesKt.accountNameCriteria(name)).and(UtilitiesKt.accountHostCriteria(host));
        try {
            List<StateAndRef<AccountInfo>> accounts = vaultService._queryBy(queryCriteria, new PageSpecification(1, 2), new Sort(Collections.emptySet()), AccountInfo.class).getStates();
            if (!accounts.isEmpty()) {
                return Optional.of(accounts.get(0).getState().getData());
            }
        } catch (VaultQueryException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

}
