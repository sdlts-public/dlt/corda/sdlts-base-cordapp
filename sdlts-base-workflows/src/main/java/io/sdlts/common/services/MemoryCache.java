package io.sdlts.common.services;

import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import io.sdlts.common.mapper.UserHelper;
import io.sdlts.common.model.UserDetails;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class MemoryCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(MemoryCache.class);

    private static final OkHttpClient client;

    private static final LoadingCache<String, UserDetails> userDetailsCache;

    static {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        client = clientBuilder.build();

        userDetailsCache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<String, UserDetails>() {
                            @Override
                            public UserDetails load(String accessToken) throws Exception {
                                return retrieveUserDetailsWithAccessToken(accessToken);
                            }
                        }
                );
    }

    private MemoryCache() {}

    private static UserDetails retrieveUserDetailsWithAccessToken(String accessToken) {
        String domainUrl = System.getenv("SDLTS_AUTH0_DOMAIN");

        if (Strings.isNullOrEmpty(domainUrl)) {
            throw new IllegalStateException("SDLTS_AUTH0_DOMAIN environment variable is missing. Please, configure node env variable.");
        }

        Optional<String> userInfoAsJson = resolveUserData(domainUrl, accessToken);

        return userInfoAsJson.map(s -> resolveUserDetails(convertToJson(s))).orElse(null);
    }

    private static Optional<String> resolveUserData(String domainUrl, String accessToken) {
        Request request = new Request.Builder()
                .url(String.format("https://%s/userinfo", domainUrl))
                .get()
                .addHeader("authorization", "Bearer " + accessToken)
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            Response response = client.newCall(request).execute();
            ResponseBody body = response.body();
            if (body != null) {
                return Optional.of(body.string());
            }
        } catch (IOException e) {
            LOGGER.error("Could not resolve body: {}", e.getMessage());
        }
        return Optional.empty();
    }

    private static JSONObject convertToJson(String userInfoAsJson) {
        try {
            return new JSONObject(userInfoAsJson);
        } catch (Exception e) {
            LOGGER.error("Could not parse json: {}", e.getMessage());
        }
        return null;
    }

    private static UserDetails resolveUserDetails(JSONObject json) {
        if (json == null) {
            return null;
        }
        String email = json.getString("email");

        if (json.has("given_name")) {
            String firstname = json.getString("given_name");
            String lastname = json.getString("family_name");
            return UserHelper.of(email, firstname, lastname, email);
        }
        String name = json.getString("name");
        return new UserDetails(email, name, email);
    }

    public static LoadingCache<String, UserDetails> getLoadingCache() {
        return userDetailsCache;
    }

}
