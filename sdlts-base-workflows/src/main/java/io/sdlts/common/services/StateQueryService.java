package io.sdlts.common.services;

import com.google.common.collect.ImmutableList;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.node.AppServiceHub;
import net.corda.core.node.services.CordaService;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.serialization.SingletonSerializeAsToken;

import java.util.List;

@CordaService
public class StateQueryService extends SingletonSerializeAsToken {
    private final AppServiceHub serviceHub;

    public StateQueryService(AppServiceHub serviceHub) {
        this.serviceHub = serviceHub;
    }


    public  <S extends ContractState> StateAndRef<S> getByLinearId(Class<S> stateClass, UniqueIdentifier linearId) {
        return getByLinearId(stateClass, linearId, false);
    }

    public  <S extends ContractState> StateAndRef<S> getByLinearId(Class<S> stateClass, UniqueIdentifier linearId, boolean optional) {
        QueryCriteria queryCriteria = new QueryCriteria.LinearStateQueryCriteria(
                null,
                ImmutableList.of(linearId),
                Vault.StateStatus.UNCONSUMED,
                null);

        List<StateAndRef<S>> contracts = serviceHub.getVaultService().queryBy(stateClass, queryCriteria).getStates();
        if (contracts.size() != 1 && !optional) {
            throw new IllegalStateException(String.format("Smart contract with id %s not found.", linearId));
        }
        return contracts.isEmpty() ? null : contracts.get(0);
    }
}
