package io.sdlts.common.services;

import com.google.common.base.Strings;
import io.sdlts.common.mapper.BlockchainAddress;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import net.corda.core.node.AppServiceHub;
import net.corda.core.node.services.CordaService;
import net.corda.core.serialization.SingletonSerializeAsToken;

@CordaService
public class BlockchainService extends SingletonSerializeAsToken {
    private final AppServiceHub serviceHub;

    public BlockchainService(AppServiceHub serviceHub) {
        this.serviceHub = serviceHub;
    }

    public BlockchainAddress resolve(String blockchainAddress) {
        BlockchainAddress temporary = BlockchainAddress.parse(blockchainAddress);
        if (temporary == null) {
            throw new IllegalStateException("Not valid blockchain address provided");
        }
        Party host = resolveHostAddress(temporary.getHostAddress());

        return BlockchainAddress.of(temporary, host);
    }

    private Party resolveHostAddress(String hostAddress) {
        if (Strings.isNullOrEmpty(hostAddress)) {
            return serviceHub.getMyInfo().getLegalIdentities().get(0);
        }
        CordaX500Name item = CordaX500Name.parse(hostAddress);
        Party party = serviceHub.getIdentityService().wellKnownPartyFromX500Name(item);
        if (party == null) {
            throw new IllegalStateException(String.format("Party was not resolved. Is it part of blockchain network(%s)?", hostAddress));
        }
        return party;
    }

    public BlockchainAddress resolve(BlockchainAddress blockchainAddress) {
        Party host = resolveHostAddress(blockchainAddress.getHostAddress());

        return BlockchainAddress.of(blockchainAddress, host);
    }
}
