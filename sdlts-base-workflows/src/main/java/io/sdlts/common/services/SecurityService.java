package io.sdlts.common.services;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import io.sdlts.common.contracts.persistence.PermissionsSchema;
import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.mapper.DecodedAuthRequest;
import io.sdlts.common.model.UserDetails;
import io.sdlts.common.queries.PermissionsQuery;
import io.sdlts.common.workflows.ErrorCodes;
import kotlin.jvm.internal.Intrinsics;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.FlowException;
import net.corda.core.identity.Party;
import net.corda.core.node.AppServiceHub;
import net.corda.core.node.services.CordaService;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.VaultService;
import net.corda.core.node.services.vault.Builder;
import net.corda.core.node.services.vault.FieldInfo;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.QueryCriteriaUtils;
import net.corda.core.serialization.SingletonSerializeAsToken;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@CordaService
public class SecurityService extends SingletonSerializeAsToken {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityService.class);

    private final AppServiceHub serviceHub;

    public SecurityService(AppServiceHub serviceHub) {
        this.serviceHub = serviceHub;
    }

    private boolean hasAccess(String username, String blockchainAddress) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(blockchainAddress), "Account name is missing");

        Party host = serviceHub.getMyInfo().getLegalIdentities().get(0);

        Intrinsics.checkParameterIsNotNull(host, "host");
        VaultService vaultService = serviceHub.getVaultService();

        QueryCriteria baseCriteria = new QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED);

        try {
            QueryCriteria queryCriteria = baseCriteria.and(usernameCriteria(username)).and(blockchainAddressCriteria(blockchainAddress));
            Vault.Page<SmartPermissionsState> results = vaultService.queryBy(SmartPermissionsState.class, queryCriteria);
            return ! results.getStates().isEmpty();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void ensureSecurity(String userIdentifier, String blockchainAddress) throws FlowException {
        if (Strings.isNullOrEmpty(userIdentifier)) {
            throw new FlowException("User information cannot be resolved.");
        }

        if (Strings.isNullOrEmpty(blockchainAddress)) {
            throw new FlowException("Blockchain address cannot be resolved.");
        }

        if (!hasAccess(userIdentifier, blockchainAddress)) {
            LOGGER.error("[{}]: User [{}] is not allowed to perform operation (Access to: [{}])!", ErrorCodes.ERROR_CODE_1, userIdentifier, blockchainAddress);
            throw new FlowException("Something wrong went. Please, contact node operator with error code: " + ErrorCodes.ERROR_CODE_1);
        }
    }


    @NotNull
    public final QueryCriteria usernameCriteria(@NotNull String username) throws NoSuchFieldException {
        FieldInfo usernameField = QueryCriteriaUtils.getField("username", PermissionsSchema.SmartPermissionsPersistence.class);
        return new QueryCriteria.VaultCustomQueryCriteria(Builder.equal(usernameField, username));
    }

    @NotNull
    public final QueryCriteria blockchainAddressCriteria(@NotNull String blockchainAddress) throws NoSuchFieldException {
        FieldInfo blockchainAddressField = QueryCriteriaUtils.getField("blockchainAddress", PermissionsSchema.SmartPermissionsPersistence.class);
        return new QueryCriteria.VaultCustomQueryCriteria(Builder.equal(blockchainAddressField, blockchainAddress));
    }

    public DecodedAuthRequest decode(AuthRequest authRequest, String simpleName) throws FlowException {
        return decode(authRequest, simpleName, null);
    }

    public DecodedAuthRequest decode(AuthRequest authRequest, String clzName, UniqueIdentifier linearIdAccessing) throws FlowException {
        if (authRequest == null) {
            throw new FlowException("Auth request is missing");
        }
        UserDetails user = new AuthDecoder(authRequest).decode();
        Optional<SmartPermissionsState> permission = new PermissionsQuery(serviceHub, user.getId()).first();

        AtomicReference<String> blockchainAddressRef = new AtomicReference<>();
        if (!Strings.isNullOrEmpty(authRequest.getBlockchainAddress())) {
            blockchainAddressRef.set(authRequest.getBlockchainAddress());
        } else {
            permission.ifPresent(state -> blockchainAddressRef.set(state.getPartyAddress()));
        }

        if (Strings.isNullOrEmpty(blockchainAddressRef.get())) {
            String additionalMessage = linearIdAccessing != null ? "and linear Id: " + linearIdAccessing : "";
            LOGGER.error("[{}]: User [{}] is not allowed to perform operation (Access to class: [{}] {})!", ErrorCodes.ERROR_CODE_1, user.getId(), clzName, additionalMessage);
            throw new FlowException("Something wrong went. Please, contact node operator with error code: " + ErrorCodes.ERROR_CODE_1);
        }
        BlockchainService blockchainService = serviceHub.cordaService(BlockchainService.class);

        BlockchainAddress address = blockchainService.resolve(blockchainAddressRef.get());

        return new DecodedAuthRequest.Builder()
                .withAuthRequest(authRequest)
                .withUser(user)
                .withAccountIdentifier(permission)
                .withBlockchainAddress(address)
                .build();
    }
}
