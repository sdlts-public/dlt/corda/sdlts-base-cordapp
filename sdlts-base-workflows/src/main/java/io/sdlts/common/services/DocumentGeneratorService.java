package io.sdlts.common.services;

import fr.opensagres.odfdom.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.ConverterTypeVia;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.odt.discovery.ODTTemplateEngineConfiguration;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.ITemplateEngine;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.freemarker.FreemarkerTemplateEngine;
import io.sdlts.common.model.FileMetadata;
import io.sdlts.common.utils.ModifiedDate;
import io.sdlts.common.utils.ZippedDocument;
import net.corda.core.contracts.Attachment;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.FlowException;
import net.corda.core.node.AppServiceHub;
import net.corda.core.node.services.CordaService;
import net.corda.core.serialization.SingletonSerializeAsToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipInputStream;

@CordaService
public class DocumentGeneratorService extends SingletonSerializeAsToken {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentGeneratorService.class);

    protected final AppServiceHub serviceHub;

    protected DocumentGeneratorService(AppServiceHub serviceHub) {
        this.serviceHub = serviceHub;
    }

    public Optional<InputStream> downloadAttachment(String attachmentHash) throws FlowException {
        InputStream is = retrieveFromDlt(attachmentHash);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try (ZipInputStream zis = new ZipInputStream(is)) {
            zis.getNextEntry();


            byte[] buffer = new byte[1024];
            int len;
            while ((len = zis.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
            }
            return Optional.of(new ByteArrayInputStream(bos.toByteArray()));
        } catch (IOException e) {
            throw new FlowException("Could not unzip attachment!", e);
        }
    }

    private InputStream retrieveFromDlt(String attachmentHash) throws FlowException {
        Attachment attachment = serviceHub.getAttachments().openAttachment(SecureHash.parse(attachmentHash));
        if (attachment == null) {
            throw new FlowException("Could not find requested attachment!");
        }
        return attachment.open();
    }

    public Optional<ZippedDocument> generate(Map<String, ?> contextValues, InputStream template) throws FlowException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            writeAsPdf(contextValues, template, out);

            String enc = Base64.getEncoder().encodeToString(out.toByteArray());
            LOGGER.info("Content size: {}", out.size());

            byte[] result = Base64.getDecoder().decode(enc);

            return Optional.of(new ZippedDocument(result).zip());
        } catch (IOException | XDocReportException e) {
            throw new FlowException("Could not generate pdf: " + e.getMessage(), e);
        } finally {
            try {
                template.close();
            } catch (IOException e) {
                LOGGER.warn("Could not close template input stream!", e);
            }
        }
    }

    private void writeAsPdf(Map<String, ?> contextValues, InputStream in, ByteArrayOutputStream out) throws IOException, XDocReportException {
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Freemarker);
        ITemplateEngine templateEngine = new FreemarkerTemplateEngine();
        templateEngine.setConfiguration( ODTTemplateEngineConfiguration.INSTANCE );
        report.setTemplateEngine(templateEngine);

        PdfOptions pdfOptions = PdfOptions.create().fontEncoding("UTF-8");
        Options options = Options.getTo(ConverterTypeTo.PDF).via(ConverterTypeVia.ODFDOM).subOptions(pdfOptions);

        // Add properties to the context
        IContext ctx = report.createContext();

        for (Map.Entry<String, ?> entry : contextValues.entrySet()) {
            ctx.put(entry.getKey(), entry.getValue());
        }

        // Write the PDF file to output stream
        report.convert(ctx, options, out);
    }

    public FileMetadata createFileMetadata(ZippedDocument document, String name, String attachmentType) {
        FileMetadata fileMetadata = new FileMetadata();


        fileMetadata.setName(name);
        fileMetadata.setLastUpdated(new ModifiedDate().asString());
        fileMetadata.setAttachmentType(attachmentType);
        fileMetadata.setSize(document.getSize());

        try {
            SecureHash secureHash = serviceHub.getAttachments().importAttachment(document.getContent(), "system", fileMetadata.getName());
            fileMetadata.setHash(secureHash.toString());

        } catch (IOException e) {
            throw new IllegalStateException("Could not upload attachment!", e);
        }

        return fileMetadata;
    }

}
