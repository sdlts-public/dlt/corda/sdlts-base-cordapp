package io.sdlts.common.services.tokens;

import com.auth0.jwt.interfaces.DecodedJWT;
import io.sdlts.common.mapper.UserHelper;
import io.sdlts.common.model.UserDetails;

public interface TokenVerifier {
    default UserDetails defaultUserDetails() {
        return new UserDetails("NA", "NA", "NA");
    }

    default UserDetails resolveUserDetails(DecodedJWT jwt) {
        String email = jwt.getClaims().get("email").asString();

        if (jwt.getClaims().get("given_name") != null) {
            String firstname = jwt.getClaims().get("given_name").asString();
            String lastname = jwt.getClaims().get("family_name").asString();
            return UserHelper.of(email, firstname, lastname, email);
        }
        String name = jwt.getClaims().get("name").asString();
        return new UserDetails(email, name, email);
    }
}
