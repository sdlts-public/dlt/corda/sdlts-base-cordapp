package io.sdlts.common.services.tokens;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.sdlts.common.model.UserDetails;

public class ServiceAccountTokenVerifier implements TokenVerifier {
    private final String token;

    public ServiceAccountTokenVerifier(String accessToken) {
        this.token = accessToken;
    }

    public UserDetails verifyAndGetUser() {
        DecodedJWT jwt = JWT.decode(this.token);
        if (jwt.getClaims().isEmpty()) {
            throw new IllegalStateException("JWT could not be validated");
        }
        return resolveUserDetails(jwt);
    }
}
