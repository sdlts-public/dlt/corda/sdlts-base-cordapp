package io.sdlts.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ModifiedDate {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public String asString() {
        return this.dateFormat.format(new Date());
    }
}
