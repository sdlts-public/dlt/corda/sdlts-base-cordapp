package io.sdlts.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZippedDocument {

    private final byte[] document;

    private long size;

    private InputStream content;

    public ZippedDocument() {
        this(new byte[0]);
    }

    public ZippedDocument(byte[] document) {
        this.document = document;
    }

    public long getSize() {
        return size;
    }

    public InputStream getContent() {
        return content;
    }

    public ZippedDocument zip() throws IOException {
        content = zipInternal();
        size = document.length;
        return this;
    }

    private InputStream zipInternal() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)) {
            ZipEntry zipEntry = new ZipEntry(String.format("document-%s.pdf", new SimpleDateFormat("yyyyMMdd-HHmmssSSS").format(new Date())));
            zipOutputStream.putNextEntry(zipEntry);
            zipOutputStream.write(document);
            zipOutputStream.closeEntry();
        }
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }
}
