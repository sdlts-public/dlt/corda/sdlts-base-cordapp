package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.contracts.states.AbstractSmartState;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.mapper.TransactionRequest;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.model.Signature;
import io.sdlts.common.services.BlockchainService;
import io.sdlts.common.workflows.PartyInformationAddressFlow;
import net.corda.core.contracts.ContractState;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@InitiatingFlow
public class UpdateSignaturesFlow<R extends TransactionRequest<?, T, R>, T extends ContractState> extends FlowLogic<List<Signature>> {

    private final R request;

    private BlockchainService blockchainService;

    public UpdateSignaturesFlow(R request) {
        this.request = request;
    }


    @Suspendable
    @Override
    public List<Signature> call() throws FlowException {
        this.blockchainService = getServiceHub().cordaService(BlockchainService.class);
        return updatePartyInformation();
    }

    @Suspendable
    private List<Signature> updatePartyInformation() throws FlowException {
        List<Signature> signingParties = request.getSigningParties();
        if (signingParties.isEmpty()) {
            signingParties = resolveSigningPartiesFromState();
        }

        if (signingParties == null || signingParties.isEmpty()) {
            return new ArrayList<>();
        }
        return updateSignatures(new ArrayList<>(signingParties));
    }

    private List<Signature> resolveSigningPartiesFromState() {
        if (request.getCurrentInputState() == null) {
            return new ArrayList<>();
        }

        T currentInputState = request.getCurrentInputState().getState().getData();
        if (!(currentInputState instanceof AbstractSmartState)) {
            return new ArrayList<>();
        }
        AbstractSmartState<?, ?> smartState = (AbstractSmartState<?, ?>)currentInputState;

        return smartState.getHeader().getSigningParties();
    }

    @Suspendable
    private List<Signature> updateSignatures(List<Signature> signingParties) throws FlowException {
        for (Signature signature : signingParties) {
            if (signature.getPartyInformation() != null) {
                continue;
            }
            resolveAddress(signature);
        }
        return signingParties;
    }

    @Suspendable
    private void resolveAddress(Signature signature) throws FlowException {
        BlockchainAddress blockchainAddress = this.blockchainService.resolve(signature.getBlockchainAddress());
        Optional<PartyInformation> pi = subFlow(new PartyInformationAddressFlow(blockchainAddress));
        pi.ifPresent(signature::setPartyInformation);
    }
}
