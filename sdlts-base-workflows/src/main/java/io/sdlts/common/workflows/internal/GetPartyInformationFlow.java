package io.sdlts.common.workflows.internal;

import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.model.PartyInformationStatus;
import io.sdlts.common.workflows.AbstractQueryFlow;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;

import java.util.Optional;

@InitiatingFlow
public class GetPartyInformationFlow extends AbstractQueryFlow<StateAndRef<PartyInformationState>> {
    public GetPartyInformationFlow(BlockchainAddress blockchainAddress) {
        super(blockchainAddress);
    }

    @Override
    protected Optional<StateAndRef<PartyInformationState>> queryStates(QueryCriteria heldByAccount) {
        Vault.Page<PartyInformationState> results = getServiceHub().getVaultService().queryBy(PartyInformationState.class, heldByAccount);

        if (results.getStates().isEmpty()) {
            return Optional.empty();
        }
        for (StateAndRef<PartyInformationState> state : results.getStates()) {
            if (PartyInformationStatus.ACTIVE.equals(state.getState().getData().getHeader().getStatus())) {
                return Optional.of(state);
            }
        }
        return Optional.empty();
    }
}
