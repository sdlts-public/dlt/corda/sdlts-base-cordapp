package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.identity.PartyAndCertificate;

import java.util.UUID;

@InitiatingFlow
public class NewKeyForAccount extends FlowLogic<PartyAndCertificate> {

    private final UUID accountId;

    public NewKeyForAccount(UUID accountId) {
        this.accountId = accountId;
    }

    @Suspendable
    @Override
    public PartyAndCertificate call() throws FlowException {
        return getServiceHub().getKeyManagementService().freshKeyAndCert(
                getOurIdentityAndCert(),
                false,
                accountId);
    }

}
