package io.sdlts.common.workflows;

public final class ErrorCodes {
    private ErrorCodes() {}

    /**
     * User is trying to access account where he does not have a permission
     */
    public static final String ERROR_CODE_1 = "SNI-ERR-8882-111-0978";


    /**
     * Account already exists
     */
    public static final String ERROR_CODE_2 = "SNI-ERR-8852-141-0378";

    /**
     * Access to different account state
     */
    public static final String ERROR_CODE_3 = "SNI-ERR-8972-241-0348";

}
