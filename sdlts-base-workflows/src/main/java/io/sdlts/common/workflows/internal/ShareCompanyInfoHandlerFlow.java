package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.contracts.states.PartyInformationState;
import net.corda.core.flows.*;
import net.corda.core.transactions.SignedTransaction;

import java.util.List;

@InitiatedBy(ShareCompanyInfoFlow.class)
public class ShareCompanyInfoHandlerFlow extends FlowLogic<PartyInformationState> {
    private final FlowSession session;

    public ShareCompanyInfoHandlerFlow(FlowSession session) {
        this.session = session;
    }

    @Suspendable
    @Override
    public PartyInformationState call() throws FlowException {
        SignedTransaction transaction = subFlow(new ReceiveTransactionFlow(session));
        List<PartyInformationState> states = transaction.getCoreTransaction().outputsOfType(PartyInformationState.class);
        if (states.isEmpty()) {
            return null;
        }
        return states.stream().findFirst().orElse(null);
    }
}
