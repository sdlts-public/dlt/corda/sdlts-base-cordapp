package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.flows.RequestKeyForAccount;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.mapper.TargetAccount;
import io.sdlts.common.workflows.FindAccountInfoFlow;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;

@InitiatingFlow
public class FindTargetAccountFlow extends FlowLogic<TargetAccount> {
    private final BlockchainAddress sourceBlockchainAddress;

    private final String targetBlockchainAddress;

    public FindTargetAccountFlow(BlockchainAddress sourceBlockchainAddress, String targetBlockchainAddress) {
        this.sourceBlockchainAddress = sourceBlockchainAddress;
        this.targetBlockchainAddress = targetBlockchainAddress;
    }


    @Suspendable
    @Override
    public TargetAccount call() throws FlowException {
        TargetAccount targetAccount =  new TargetAccount();

        if (targetBlockchainAddress.equalsIgnoreCase(sourceBlockchainAddress.format())) {
            return null;
        }
        AccountInfo accountInfo = subFlow(new FindAccountInfoFlow(BlockchainAddress.parse(targetBlockchainAddress)));

        targetAccount.setAccountInfo(accountInfo);
        targetAccount.setTargetAcctAnonymousParty(subFlow(new RequestKeyForAccount(targetAccount.getAccountInfo())));
        return targetAccount;
    }
}
