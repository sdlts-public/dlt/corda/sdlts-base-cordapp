package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.flows.CreateAccount;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.model.UserDetails;
import io.sdlts.common.queries.MyPartyInformationFilter;
import io.sdlts.common.queries.MyPartyInformationQuery;
import io.sdlts.common.services.AccountInfoService;
import io.sdlts.common.services.AccountNumberGenerator;
import io.sdlts.common.services.AuthDecoder;
import io.sdlts.common.workflows.internal.AssignAccountInfoToUserFlow;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

@InitiatingFlow
@StartableByRPC
@StartableByService
public class SignupFlow extends FlowLogic<PartyInformationState> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignupFlow.class);

    private final AuthRequest authRequest;
    private final String accountName;

    private final boolean generateAccountNumber;

    public SignupFlow(AuthRequest authRequest, String accountName) {
        this(authRequest, accountName, true);
    }
    public SignupFlow(AuthRequest authRequest, String accountName, boolean generateAccountNumber) {
        this.authRequest = authRequest;
        this.accountName = accountName;
        this.generateAccountNumber = generateAccountNumber;
    }

    @Override
    @Suspendable
    public PartyInformationState call() throws FlowException {
        Preconditions.checkNotNull(this.authRequest);
        Preconditions.checkArgument(!Strings.isNullOrEmpty(this.accountName), "Account name is missing");

        UserDetails user = new AuthDecoder(this.authRequest).decode();

        MyPartyInformationFilter filter = new MyPartyInformationFilter();
        filter.setName(this.accountName);
        Optional<PartyInformation> existingName = new MyPartyInformationQuery(this.authRequest, getServiceHub(), filter).first();
        if (existingName.isPresent()) {
            throw new FlowException("Such name already exists");
        }

        String accountNumber = this.generateAccountNumber ? new AccountNumberGenerator().generate() : this.accountName.trim().replaceAll("[^A-Za-z0-9 ]", "");

        AccountInfoService accountInfoService = getServiceHub().cordaService(AccountInfoService.class);

        if (accountInfoService.getAccount(this.accountName).isPresent()) {
            LOGGER.error("[{}]: User [{}] is trying to create new account with existing name: {}",
                    ErrorCodes.ERROR_CODE_2,
                    user.getId(),
                    this.accountName);
            throw new FlowException("Something wrong went. Please, contact node operator with error code: " + ErrorCodes.ERROR_CODE_2);

        }

        StateAndRef<? extends AccountInfo> accountInfoStateAndRef = subFlow(new CreateAccount(accountNumber));

        UniqueIdentifier accountIdentifier = accountInfoStateAndRef.getState().getData().getIdentifier();

        return subFlow(new AssignAccountInfoToUserFlow(user.getId(), accountIdentifier,  accountNumber, this.accountName))
                .orElseThrow(() -> new FlowException("Could not fing party information details"));
    }
}
