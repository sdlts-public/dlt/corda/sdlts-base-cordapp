package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.flows.ShareAccountInfoHandlerFlow;
import io.sdlts.common.mapper.BlockchainAddress;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.identity.AnonymousParty;
import net.corda.core.utilities.UntrustworthyData;

@InitiatingFlow
public class RequestAccountInfoFlow extends FlowLogic<AccountInfo> {

    private final BlockchainAddress blockchainAddress;

    public RequestAccountInfoFlow(BlockchainAddress blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    @Suspendable
    @Override
    public AccountInfo call() throws FlowException {
        FlowSession session = initiateFlow(new AnonymousParty(this.blockchainAddress.getHost().getOwningKey()));
        String accountNumber = this.blockchainAddress.getAccountNumber();
        UntrustworthyData<Boolean> untrustworthyData = session.sendAndReceive(Boolean.class, accountNumber);

        boolean hasAccount = untrustworthyData.unwrap(msg -> msg);

        return hasAccount ? subFlow(new ShareAccountInfoHandlerFlow(session)) : null;
    }
}
