package io.sdlts.common.workflows.internal;

import com.google.common.base.Strings;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import io.sdlts.common.DefaultRoles;
import io.sdlts.common.contracts.PartyInformationContract;
import io.sdlts.common.contracts.PermissionContract;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.AccountInfoTransactionRequest;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.mapper.StateCommandPair;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.model.PartyInformationHeader;
import io.sdlts.common.workflows.AbstractTransactionalSecuredFlow;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.identity.PartyAndCertificate;

import java.util.Date;
import java.util.Optional;

@InitiatingFlow
public class AssignAccountInfoToUserFlow extends AbstractTransactionalSecuredFlow<AccountInfoTransactionRequest, AccountInfo, Optional<PartyInformationState>> {
    private final String username;

    private final UniqueIdentifier accountIdentifier;

    private final String accountNumber;

    private final String accountName;

    private BlockchainAddress blockchainAddress;

    public AssignAccountInfoToUserFlow(String username, UniqueIdentifier accountIdentifier, String accountNumber, String accountName) {
        super(AuthRequest.of(username), new AccountInfoTransactionRequest());
        this.username = username;
        this.accountIdentifier = accountIdentifier;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
    }

    @Override
    protected boolean onBeforeStart(AccountInfoTransactionRequest request) throws FlowException {
        PartyAndCertificate partyAndCertificate =  subFlow(new NewKeyForAccount(this.accountIdentifier.getId()));
        request.setMyAccountPublicKey(partyAndCertificate.getOwningKey());
        this.blockchainAddress = BlockchainAddress.of(this.accountNumber, getOurIdentity());

        return true;
    }

    @Override
    protected void createCurrentOutputState(AccountInfoTransactionRequest request) {
        request.setCurrentOutputState(new StateCommandPair(
                new SmartPermissionsState(this.accountIdentifier,
                        this.username,
                        this.blockchainAddress.format(),
                        DefaultRoles.ADMIN,
                        request.getParticipants()),
                new PermissionContract.Commands.AssignAccountToUser()));
    }

    @Override
    protected Optional<PartyInformationState> result(AccountInfoTransactionRequest request) {
        if (!request.getOtherOutputStates().isEmpty()) {
            return Optional.of((PartyInformationState)request.getOtherOutputStates().get(0).getState());
        }
        return Optional.empty();
    }

    @Override
    protected void createOtherOutputStates(AccountInfoTransactionRequest request) {
        if (Strings.isNullOrEmpty(this.accountName)) {
            return;
        }
        PartyInformation content = new PartyInformation();
        content.setName(this.accountName);
        content.setBlockchainAddress(this.blockchainAddress.format());
        PartyInformationHeader header = new PartyInformationHeader.Builder()
                .createDate(new Date())
                .creatorBlockchainAddress(this.blockchainAddress.format())
                .participants(request.getParticipants()).build();

        request.getOtherOutputStates().add(new StateCommandPair(
                new PartyInformationState(header, content),
                new PartyInformationContract.Commands.NewAddress()));
    }

    @Override
    protected Class<AccountInfo> getImplementingClass() {
        return AccountInfo.class;
    }
}
