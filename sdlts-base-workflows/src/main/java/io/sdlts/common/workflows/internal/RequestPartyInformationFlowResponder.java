package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.BlockchainAddress;
import kotlin.Unit;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatedBy;
import net.corda.core.utilities.UntrustworthyData;

import java.util.Optional;

@InitiatedBy(RequestPartyInformationFlow.class)
public class RequestPartyInformationFlowResponder extends FlowLogic<Unit> {

    private FlowSession counterpartySession;

    public RequestPartyInformationFlowResponder(FlowSession counterpartySession) {
        this.counterpartySession = counterpartySession;
    }

    @Suspendable
    @Override
    public Unit call() throws FlowException {
        UntrustworthyData<String> response = counterpartySession.receive(String.class);

        String accountNumber = response.getFromUntrustedWorld();
        BlockchainAddress address = BlockchainAddress.of(accountNumber, getOurIdentity());

        Optional<StateAndRef<PartyInformationState>> partyInformationStateStateAndRef = subFlow(new GetPartyInformationFlow(address));

        if (partyInformationStateStateAndRef.isPresent()) {
            counterpartySession.send(true);
            return subFlow(new ShareCompanyInfoFlow(partyInformationStateStateAndRef.get(), ImmutableList.of(counterpartySession)));

        } else {
            counterpartySession.send(false);
        }
        return null;
    }
}
