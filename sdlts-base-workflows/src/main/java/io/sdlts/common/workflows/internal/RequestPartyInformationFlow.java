package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.BlockchainAddress;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.identity.AnonymousParty;
import net.corda.core.utilities.UntrustworthyData;

@InitiatingFlow
public class RequestPartyInformationFlow extends FlowLogic<PartyInformationState> {

    private final BlockchainAddress blockchainAddress;

    public RequestPartyInformationFlow(BlockchainAddress blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    @Suspendable
    @Override
    public PartyInformationState call() throws FlowException {
        FlowSession session = initiateFlow(new AnonymousParty(this.blockchainAddress.getHost().getOwningKey()));

        UntrustworthyData<Boolean> untrustworthyData = session.sendAndReceive(Boolean.class, this.blockchainAddress.getAccountNumber());

        boolean hasAccount = untrustworthyData.unwrap(msg -> msg);

        return hasAccount ? subFlow(new ShareCompanyInfoHandlerFlow(session)) : null;
    }

}
