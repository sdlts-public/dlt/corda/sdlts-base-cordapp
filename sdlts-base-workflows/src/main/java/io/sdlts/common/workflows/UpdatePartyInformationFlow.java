package io.sdlts.common.workflows;

import com.google.common.base.Strings;
import io.sdlts.common.contracts.PartyInformationContract;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.PartyInformationTransactionRequest;
import io.sdlts.common.mapper.StateCommandPair;
import io.sdlts.common.model.*;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.flows.StartableByService;
import net.corda.core.transactions.SignedTransaction;

@InitiatingFlow
@StartableByRPC
@StartableByService
public class UpdatePartyInformationFlow extends AbstractTransactionalSecuredFlow<PartyInformationTransactionRequest, PartyInformationState, SignedTransaction> {

    public UpdatePartyInformationFlow(AuthRequest authRequest, PartyInformation partyInformation, UniqueIdentifier uniqueIdentifier) {
        super(authRequest, new PartyInformationTransactionRequest().content(partyInformation).id(uniqueIdentifier));
    }

    @Override
    protected SignedTransaction result(PartyInformationTransactionRequest request) {
        return request.getSignedTransaction();
    }

    @Override
    protected void createCurrentOutputState(PartyInformationTransactionRequest request) {
        PartyInformationState inputState = request.getCurrentInputState().getState().getData();

        String blockchainAddress = inputState.getHeader().getCreatorBlockchainAddress();
        request.getContent().setBlockchainAddress(blockchainAddress);

        //Cannot modify name at the moment
        request.getContent().setName(inputState.getContent().getName());

        PartyInformationHeader header = new PartyInformationHeader.Builder()
                .createDate(inputState.getHeader().getCreateDate())
                .creatorBlockchainAddress(blockchainAddress)
                .participants(request.getParticipants())
                .status(calculateStatus(request.getContent()))
                .build();

        request.setCurrentOutputState(new StateCommandPair(
                new PartyInformationState(header, request.getContent(), inputState.getLinearId()),
                new PartyInformationContract.Commands.UpdateAddress()));
    }

    protected PartyInformationStatus calculateStatus(PartyInformation content) {
        if ((!Strings.isNullOrEmpty(content.getVat()) || !Strings.isNullOrEmpty(content.getCompanyIdNumber())) && isCompletedAddress(content.getAddress())) {
            return PartyInformationStatus.ACTIVE;
        }
        return PartyInformationStatus.INCOMPLETE;
    }

    private boolean isCompletedAddress(Address address) {
        return !Strings.isNullOrEmpty(address.getStreet())
                && !Strings.isNullOrEmpty(address.getZip())
                && !Strings.isNullOrEmpty(address.getCity())
                && Country.isValid(address.getCountry());
    }

    @Override
    protected Class<PartyInformationState> getImplementingClass() {
        return PartyInformationState.class;
    }
}
