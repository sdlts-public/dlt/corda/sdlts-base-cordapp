package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.services.AccountService;
import com.r3.corda.lib.accounts.workflows.services.KeyManagementBackedAccountService;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.services.BlockchainService;
import io.sdlts.common.workflows.internal.RequestAccountInfoFlow;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@InitiatingFlow
@StartableByRPC
@StartableByService
public class FindAccountInfoFlow extends FlowLogic<AccountInfo> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FindAccountInfoFlow.class);

    private final BlockchainAddress blockchainAddress;

    public FindAccountInfoFlow(BlockchainAddress blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    @Suspendable
    @Override
    public AccountInfo call() throws FlowException {
        AccountService accountService = getServiceHub().cordaService(KeyManagementBackedAccountService.class);
        BlockchainService blockchainService = getServiceHub().cordaService(BlockchainService.class);

        BlockchainAddress fullAddress = null;

        try {
            fullAddress = blockchainService.resolve(this.blockchainAddress);
        } catch (IllegalStateException e) {
            LOGGER.warn("Blockchain address not found: {}", this.blockchainAddress.format(), e);
            return null;
        }

        List<StateAndRef<AccountInfo>> stateAndRefList = accountService.accountInfo(fullAddress.getAccountNumber());
        if (!stateAndRefList.isEmpty()) {
            return stateAndRefList.get(0).getState().getData();
        } else {
            if (getOurIdentity().anonymise().equals(fullAddress.getHost().anonymise())) {
                return null;
            }
            return subFlow(new RequestAccountInfoFlow(fullAddress));
        }
    }
}
