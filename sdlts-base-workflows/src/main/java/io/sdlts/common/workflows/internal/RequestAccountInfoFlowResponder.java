package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.flows.ShareAccountInfoFlow;
import com.r3.corda.lib.accounts.workflows.services.AccountService;
import com.r3.corda.lib.accounts.workflows.services.KeyManagementBackedAccountService;
import kotlin.Unit;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatedBy;
import net.corda.core.utilities.UntrustworthyData;

import java.util.List;

@InitiatedBy(RequestAccountInfoFlow.class)
public class RequestAccountInfoFlowResponder extends FlowLogic<Unit> {

    private final FlowSession counterpartySession;

    public RequestAccountInfoFlowResponder(FlowSession counterpartySession) {
        this.counterpartySession = counterpartySession;
    }

    @Suspendable
    @Override
    public Unit call() throws FlowException {
        AccountService accountService = getServiceHub().cordaService(KeyManagementBackedAccountService.class);
        UntrustworthyData<String> response = counterpartySession.receive(String.class);


        List<StateAndRef<AccountInfo>> account = response.unwrap(accountService::accountInfo);
        if (account == null || account.isEmpty()) {
            counterpartySession.send(false);
        } else {
            counterpartySession.send(true);
            return subFlow(new ShareAccountInfoFlow(account.get(0), ImmutableList.of(counterpartySession)));
        }
        return null;
    }

}
