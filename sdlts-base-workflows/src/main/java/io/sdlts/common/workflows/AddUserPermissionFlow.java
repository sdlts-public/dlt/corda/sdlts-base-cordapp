package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.base.Strings;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import io.sdlts.common.DefaultRoles;
import io.sdlts.common.contracts.PermissionContract;
import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.AccountInfoTransactionRequest;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.mapper.StateCommandPair;
import io.sdlts.common.queries.PermissionsQuery;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.flows.StartableByService;
import net.corda.core.transactions.SignedTransaction;

import java.util.List;

@InitiatingFlow
@StartableByRPC
@StartableByService
public class AddUserPermissionFlow extends AbstractTransactionalSecuredFlow<AccountInfoTransactionRequest, AccountInfo, SignedTransaction> {
    private final String username;

    private final String role;

    public AddUserPermissionFlow(AuthRequest authRequest, String username, String role) {
        super(authRequest, new AccountInfoTransactionRequest());
        this.username = username;
        this.role = role;
    }

    @Override
    protected SignedTransaction result(AccountInfoTransactionRequest request) {
        return request.getSignedTransaction();
    }

    @Override
    @Suspendable
    protected void createCurrentOutputState(AccountInfoTransactionRequest request) throws FlowException {
        if (Strings.isNullOrEmpty(this.role)) {
            throw new FlowException("Role is missing.");
        }
        BlockchainAddress myAddress = getCurrentAuthRequest().getBlockchainAddress();

        List<SmartPermissionsState> permissions = new PermissionsQuery(getServiceHub(), getCurrentAuthRequest().getUser().getId(), myAddress.format()).list();
        if (permissions.isEmpty() || !DefaultRoles.ADMIN.equalsIgnoreCase(permissions.get(0).getRole())) {
            throw new FlowException("User does not have a permission to perform the action");
        }

        AccountInfo accountInfo = subFlow(new FindAccountInfoFlow(myAddress));

        UniqueIdentifier uniqueIdentifier = accountInfo.getIdentifier();
        request.setCurrentOutputState(new StateCommandPair(
                new SmartPermissionsState(uniqueIdentifier, this.username, myAddress.toString(), this.role, request.getParticipants()),
                new PermissionContract.Commands.AssignAccountToUser()));
    }

    @Override
    protected Class<AccountInfo> getImplementingClass() {
        return AccountInfo.class;
    }
}
