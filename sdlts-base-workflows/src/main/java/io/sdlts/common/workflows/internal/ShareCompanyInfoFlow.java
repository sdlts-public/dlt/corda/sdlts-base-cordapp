package io.sdlts.common.workflows.internal;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.contracts.states.PartyInformationState;
import kotlin.Unit;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.*;
import net.corda.core.transactions.SignedTransaction;

import java.util.List;

@InitiatingFlow
public class ShareCompanyInfoFlow extends FlowLogic<Unit> {
    private final StateAndRef<PartyInformationState> partyInformationStateStateAndRef;
    private final List<FlowSession> recipients;

    public ShareCompanyInfoFlow(StateAndRef<PartyInformationState> partyInformationStateStateAndRef, List<FlowSession> recipients) {
        this.partyInformationStateStateAndRef = partyInformationStateStateAndRef;
        this.recipients = recipients;
    }

    @Suspendable
    @Override
    public Unit call() throws FlowException {
        SecureHash txHash = partyInformationStateStateAndRef.getRef().getTxhash();
        SignedTransaction transactionToSend = getServiceHub().getValidatedTransactions().getTransaction(txHash);
        for (FlowSession session : recipients) {
            subFlow(new SendTransactionFlow(session, transactionToSend));
        }
        return null;
    }
}
