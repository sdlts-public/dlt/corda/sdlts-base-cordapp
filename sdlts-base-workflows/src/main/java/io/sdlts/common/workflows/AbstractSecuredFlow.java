package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.DecodedAuthRequest;
import io.sdlts.common.services.SecurityService;
import io.sdlts.common.services.StateQueryService;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;

public abstract class AbstractSecuredFlow<T> extends FlowLogic<T> {
    private final AuthRequest authRequest;

    private DecodedAuthRequest decodedAuthRequest;

    private StateQueryService stateQueryService;

    protected AbstractSecuredFlow(AuthRequest authRequest) {
        this.authRequest = authRequest;

    }

    @Suspendable
    @Override
    public final T call() throws FlowException {
        SecurityService securityService = getServiceHub().cordaService(SecurityService.class);
        this.decodedAuthRequest = securityService.decode(this.authRequest, this.getClass().getSimpleName());
        securityService.ensureSecurity(decodedAuthRequest.getUser().getId(), decodedAuthRequest.getBlockchainAddress().format());
        return onCallStarted();
    }

    protected DecodedAuthRequest getCurrentAuthRequest() {
        return this.decodedAuthRequest;
    }

    @Suspendable
    protected abstract T onCallStarted() throws FlowException;

    public StateQueryService getStateQueryService() {
        if (this.stateQueryService == null) {
            this.stateQueryService = getServiceHub().cordaService(StateQueryService.class);
        }
        return this.stateQueryService;
    }

}
