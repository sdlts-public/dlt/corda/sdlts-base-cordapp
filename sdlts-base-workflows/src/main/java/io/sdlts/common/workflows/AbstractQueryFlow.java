package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import io.sdlts.common.mapper.BlockchainAddress;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.node.services.vault.QueryCriteria;

import java.util.Collections;
import java.util.Optional;

public abstract class AbstractQueryFlow<T> extends FlowLogic<Optional<T>> {

    private final BlockchainAddress blockchainAddress;

    protected AbstractQueryFlow(BlockchainAddress blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    @Suspendable
    @Override
    public final Optional<T> call() throws FlowException {
        QueryCriteria heldByAccount = prepareBaseQuery(subFlow(new FindAccountInfoFlow(this.blockchainAddress)));

        if (heldByAccount != null) {
            return queryStates(heldByAccount);
        }
        return Optional.empty();
    }

    private QueryCriteria prepareBaseQuery(AccountInfo accountInfo) {
        if (accountInfo == null) {
            return null;
        }
        return new QueryCriteria.VaultQueryCriteria().withExternalIds(Collections.singletonList(accountInfo.getIdentifier().getId()));
    }

    protected abstract Optional<T> queryStates(QueryCriteria heldByAccount);
}
