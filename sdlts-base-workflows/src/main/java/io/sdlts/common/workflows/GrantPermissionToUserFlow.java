package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.base.Strings;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import io.sdlts.common.contracts.PartyInformationContract;
import io.sdlts.common.contracts.PermissionContract;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.*;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.model.PartyInformationHeader;
import io.sdlts.common.model.PartyInformationStatus;
import io.sdlts.common.workflows.internal.FindTargetAccountFlow;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.flows.StartableByService;
import net.corda.core.transactions.SignedTransaction;

import java.util.Date;

@InitiatingFlow
@StartableByRPC
@StartableByService
public class GrantPermissionToUserFlow extends AbstractTransactionalSecuredFlow<AccountInfoTransactionRequest, AccountInfo, SignedTransaction> {
    private final String username;

    private final String blockchainAccountAddress;

    private final String role;

    public GrantPermissionToUserFlow(AuthRequest authRequest, String username, String blockchainAccountAddress, String role) {
        super(authRequest, new AccountInfoTransactionRequest());
        this.username = username;
        this.blockchainAccountAddress = blockchainAccountAddress;
        this.role = role;
    }

    @Override
    protected SignedTransaction result(AccountInfoTransactionRequest request) {
        return request.getSignedTransaction();
    }

    @Override
    @Suspendable
    protected void createCurrentOutputState(AccountInfoTransactionRequest request) throws FlowException {
        TargetAccount targetAccount = subFlow(new FindTargetAccountFlow(getCurrentAuthRequest().getBlockchainAddress(), this.blockchainAccountAddress));

        if (targetAccount == null) {
            throw new FlowException("Account identifier is missing.");
        }
        if (Strings.isNullOrEmpty(this.role)) {
            throw new FlowException("Role is missing.");
        }

        UniqueIdentifier uniqueIdentifier = targetAccount.getAccountInfo().getIdentifier();
        request.setCurrentOutputState(new StateCommandPair(
                new SmartPermissionsState(uniqueIdentifier, this.username, this.blockchainAccountAddress, this.role, request.getParticipants()),
                new PermissionContract.Commands.AssignAccountToUser()));
    }

    @Override
    @Suspendable
    protected void createOtherOutputStates(AccountInfoTransactionRequest request) {
        PartyInformationHeader header = new PartyInformationHeader.Builder()
                .createDate(new Date())
                .status(PartyInformationStatus.READ)
                .creatorBlockchainAddress(this.blockchainAccountAddress)
                .signingParties(request.getSigningParties())
                .participants(request.getParticipants()).build();

        PartyInformation content = new PartyInformation();

        content.setBlockchainAddress(blockchainAccountAddress);
        content.setName(BlockchainAddress.parse(this.blockchainAccountAddress).getAccountNumber());

        PartyInformationState partyInformationState = new PartyInformationState(header, content);

        request.getOtherOutputStates().add(new StateCommandPair(partyInformationState, new PartyInformationContract.Commands.NewAddress()));
    }

    @Override
    protected Class<AccountInfo> getImplementingClass() {
        return AccountInfo.class;
    }
}
