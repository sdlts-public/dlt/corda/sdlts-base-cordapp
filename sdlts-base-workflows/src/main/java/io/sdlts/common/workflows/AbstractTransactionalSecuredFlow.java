package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.flows.ShareStateAndSyncAccounts;
import io.sdlts.common.contracts.states.AbstractSmartState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.mapper.DecodedAuthRequest;
import io.sdlts.common.mapper.TargetAccount;
import io.sdlts.common.mapper.TransactionRequest;
import io.sdlts.common.services.SecurityService;
import io.sdlts.common.workflows.internal.NewKeyForAccount;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.CollectSignaturesFlow;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowSession;
import net.corda.core.identity.Party;
import net.corda.core.identity.PartyAndCertificate;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTransactionalSecuredFlow<R extends TransactionRequest<?, T, R>, T extends ContractState, S> extends AbstractTransactionalFlow<R, T, S> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTransactionalSecuredFlow.class);

    private final AuthRequest authRequest;

    private DecodedAuthRequest decodedAuthRequest;

    protected AbstractTransactionalSecuredFlow(AuthRequest authRequest, R request) {
        super(request);
        this.authRequest = authRequest;
    }

    @Override
    protected DecodedAuthRequest getCurrentAuthRequest() {
        return this.decodedAuthRequest;
    }

    protected AuthRequest getAuthRequest() {
        return this.authRequest;
    }

    @Suspendable
    @Override
    protected boolean onBeforeStart(R request) throws FlowException {

        SecurityService securityService = getServiceHub().cordaService(SecurityService.class);
        this.decodedAuthRequest = securityService.decode(this.authRequest, this.getClass().getSimpleName(), request.getLinearId());
        securityService.ensureSecurity(decodedAuthRequest.getUser().getId(), decodedAuthRequest.getBlockchainAddress().format());

        AccountInfo myAccountInfo = subFlow(new FindAccountInfoFlow(decodedAuthRequest.getBlockchainAddress()));
        PartyAndCertificate partyAndCertificate =  subFlow(new NewKeyForAccount(myAccountInfo.getIdentifier().getId()));
        request.setMyAccountPublicKey(partyAndCertificate.getOwningKey());

        return true;
    }

    @Override
    protected final void onValidateCurrentInputState(R request) throws FlowException {
        if (request.getCurrentInputState() == null) {
            return;
        }
        T inputState = request.getCurrentInputState().getState().getData();
        if (inputState instanceof AbstractSmartState) {
            AbstractSmartState<?, ?> abstractSmartState = (AbstractSmartState<?, ?>) inputState;
            String currentBlochainAddress = getCurrentAuthRequest().getBlockchainAddress().format();
            long signingPartyExists = abstractSmartState.getHeader().getSigningParties().stream().filter(i ->i.getBlockchainAddress().equalsIgnoreCase(currentBlochainAddress)).count();

            if (signingPartyExists < 1 && !abstractSmartState.getHeader().getCreatorBlockchainAddress().equalsIgnoreCase(currentBlochainAddress)) {
                LOGGER.error("[{}]: User [{}] from account [{}] is trying to access state [{}] on account [{}]",
                        ErrorCodes.ERROR_CODE_3,
                        this.decodedAuthRequest.getUser().getId(),
                        currentBlochainAddress,
                        abstractSmartState.getClass().getSimpleName(),
                        abstractSmartState.getHeader().getCreatorBlockchainAddress());
                throw new FlowException("Something wrong went with code: " + ErrorCodes.ERROR_CODE_3);
            }
        }
    }

    @Override
    @Suspendable
    protected void signInitialTransaction(R request, TransactionBuilder transactionBuilder) {
        request.setSignedTransaction(
                getServiceHub().signInitialTransaction(
                    transactionBuilder,
                    ImmutableList.of(getOurIdentity().getOwningKey(), request.getMyAccountPublicKey())
                )
        );
    }

    @Override
    @Suspendable
    protected void collectSignatures(R request) throws FlowException {

        if (request.getParticipants().size() < 2) {
            return;
        }
        List<FlowSession> sessionForAccountToSendTos = new ArrayList<>();
        List<Party> targets = new ArrayList<>();
        for (TargetAccount target : request.getTargetAccounts()) {
            Party targetParty = target.getAccountInfo().getHost();
            if (targets.contains(targetParty)) {
                continue;
            }
            FlowSession sessionForAccountToSendTo = initiateFlow(targetParty);
            sessionForAccountToSendTos.add(sessionForAccountToSendTo);
            targets.add(targetParty);
        }
        SignedTransaction signedByCounterParty = subFlow(new CollectSignaturesFlow(request.getSignedTransaction(), sessionForAccountToSendTos, request.getParticipantKeys()));

        List<FlowSession> involvedParties = new ArrayList<>();
        for (FlowSession flowSession : sessionForAccountToSendTos) {
            if (!flowSession.getCounterparty().equals(getOurIdentity())) {
                involvedParties.add(flowSession);
            }
        }

        request.setSignedTransaction(signedByCounterParty);
        request.setTargetSessions(involvedParties);

    }

    @Suspendable
    @Override
    protected void onAfterNotarise(R request) throws FlowException {
        for (FlowSession targetSession : request.getTargetSessions()) {
            StateAndRef<T> stateAndRef = request.getSignedTransaction().getTx().outRefsOfType(getImplementingClass()).get(0);

            Party targetParty = getServiceHub().getIdentityService().wellKnownPartyFromAnonymous(targetSession.getCounterparty());
            if (targetParty == null) {
                throw new FlowException("Target party from anonymous could not be found!");
            }
            subFlow(new ShareStateAndSyncAccounts(stateAndRef, targetParty));
        }
    }


}
