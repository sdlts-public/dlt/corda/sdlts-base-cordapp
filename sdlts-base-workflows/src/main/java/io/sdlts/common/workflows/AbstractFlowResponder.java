package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import com.r3.corda.lib.accounts.workflows.services.AccountService;
import com.r3.corda.lib.accounts.workflows.services.KeyManagementBackedAccountService;
import io.sdlts.common.contracts.states.AbstractSmartState;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.model.Signature;
import io.sdlts.common.services.BlockchainService;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class AbstractFlowResponder<T extends AbstractSmartState> extends FlowLogic<SignedTransaction> {
    private FlowSession counterpartySession;

    protected AbstractFlowResponder(FlowSession counterpartySession) {
        this.counterpartySession = counterpartySession;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        AccountService accountService = getServiceHub().cordaService(KeyManagementBackedAccountService.class);
        BlockchainService blockchainService = getServiceHub().cordaService(BlockchainService.class);

        SignTransactionFlow signTransactionFlow = new SignTransactionFlow(counterpartySession) {
            @Override
            protected void checkTransaction(@NotNull SignedTransaction stx) throws FlowException {
                T contractingState = stx.getCoreTransaction().outRefsOfType(getImplementingClass()).get(0).getState().getData();
                for (Signature signature : contractingState.getHeader().getSigningParties()) {
                    BlockchainAddress address = blockchainService.resolve(signature.getBlockchainAddress());
                    Party party = address.getHost();
                    if (getOurIdentity().getName().equals(party.getName())) {
                        List<StateAndRef<AccountInfo>> stateAndRef = accountService.accountInfo(address.getAccountNumber());
                        if (stateAndRef.isEmpty()) {
                            throw new FlowException("Account was not found on this node");
                        }
                    }
                }

            }
        };

        SignedTransaction transaction = subFlow(signTransactionFlow);
        if (!counterpartySession.getCounterparty().equals(getOurIdentity())) {
            subFlow(new ReceiveFinalityFlow(counterpartySession, transaction.getId()));
        }
        onSignedTransaction(transaction);
        return null;
    }

    @Suspendable
    protected void onSignedTransaction(SignedTransaction transaction) throws FlowException {

    }

    protected abstract Class<T> getImplementingClass();
}
