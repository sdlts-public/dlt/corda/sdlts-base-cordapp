package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.contracts.states.AbstractSmartState;
import io.sdlts.common.mapper.DecodedAuthRequest;
import io.sdlts.common.mapper.StateCommandPair;
import io.sdlts.common.mapper.TargetAccount;
import io.sdlts.common.mapper.TransactionRequest;
import io.sdlts.common.model.FileMetadata;
import io.sdlts.common.model.Signature;
import io.sdlts.common.model.StringPair;
import io.sdlts.common.services.BlockchainService;
import io.sdlts.common.services.StateQueryService;
import io.sdlts.common.workflows.internal.FindTargetAccountFlow;
import io.sdlts.common.workflows.internal.UpdateSignaturesFlow;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.FinalityFlow;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.AnonymousParty;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractTransactionalFlow<R extends TransactionRequest<?, T, R>, T extends ContractState, S> extends FlowLogic<S> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTransactionalFlow.class);

    private StateQueryService stateQueryService;

    private BlockchainService blockchainService;

    private final R request;

    protected AbstractTransactionalFlow(R request) {
        this.request = request;
    }

    @Suspendable
    @Override
    public final S call() throws FlowException {
        return makeTransaction(this.request);
    }

    @Suspendable
    protected S makeTransaction(R request) throws FlowException {
        if (!onBeforeStart(request)) {
            return null;
        }

        fetchCurrentInputState(request);

        onValidateCurrentInputState(request);

        onAfterCurrentInputStateResolved (request);

        createOtherInputStates(request);

        resolveSigningParties(request);// ???

        resolveParticipantKeys(request); // ???

        // Resolve list of participants
        resolveParticipants(request); // ???

        onBeforeOutputStatesCreate(request);

        signIfNecessary (request);

        createCurrentOutputState(request);

        createOtherOutputStates(request);

        onBeforeTransaction(request);

        TransactionBuilder transactionBuilder = buildTransaction(request);

        // Verify the transaction
        transactionBuilder.verify(getServiceHub());

        // Sign the transaction
        signInitialTransaction(request, transactionBuilder);


        collectSignatures(request);

        // Notarise the transaction and record the state in the ledger.
        notarise(request);

        onAfterNotarise (request);

        return result(request);
    }

    protected void onValidateCurrentInputState(R request) throws FlowException {
        //Custom login to be added if necessary to validate current input state
    }

    protected abstract S result(R request);

    @Suspendable
    protected void signIfNecessary(R request) {
        if (!request.isIncludeSigningParties()) {
            return;
        }
        Optional<Signature> mySignatureOptional = getMySignature(request.getSigningParties());
        if (!mySignatureOptional.isPresent()) {
            return;
        }
        Signature mySignature = mySignatureOptional.get();
        mySignature.setSigned(true);
        mySignature.setSignatureDate(new SimpleDateFormat("dd/MM/yyyy HH:mm [z]").format(new Date()));
        mySignature.setSignedBy(getCurrentAuthRequest().toAuthenticatedUser());

        if (allPartiesSigned(request.getSigningParties())) {
            request.setAllPartiesSigned(true);
        }
    }

    @Suspendable
    protected void onBeforeTransaction(R request) throws FlowException {

    }

    @Suspendable
    protected void onAfterCurrentInputStateResolved(R request) throws FlowException {

    }

    @Suspendable
    protected void onAfterNotarise(R request) throws FlowException {

    }

    @Suspendable
    protected void onBeforeOutputStatesCreate(R request) {

    }

    @Suspendable
    private void resolveParticipantKeys(R request) throws FlowException {
        if (request.getMyAccountPublicKey() == null) {
            return;
        }
        List<PublicKey> allKeys = new ArrayList<>();
        allKeys.add(request.getMyAccountPublicKey());

        if (!request.isIncludeSigningParties()) {
            request.setParticipantKeys(allKeys);
            return;
        }
        List<TargetAccount> targetAccounts = new ArrayList<>();

        DecodedAuthRequest currentAuthRequest = getCurrentAuthRequest();
        if (currentAuthRequest == null) {
            LOGGER.warn("Flow authentication failed.");
            return;
        }

        StateAndRef<T> currentInputStateAndRef = request.getCurrentInputState();
        if (currentInputStateAndRef == null) {
            tryResolveParticipantKeys(allKeys, targetAccounts, request.getSigningParties());
            return;
        }
        T currentInputState = currentInputStateAndRef.getState().getData();
        if (!(currentInputState instanceof AbstractSmartState)) {
            return;
        }
        AbstractSmartState<?,?> smartState = (AbstractSmartState<?,?>)currentInputState;

        tryResolveParticipantKeys(allKeys, targetAccounts, smartState.getHeader().getSigningParties());
    }

    @Suspendable
    private void tryResolveParticipantKeys(List<PublicKey> allKeys, List<TargetAccount> targetAccounts, List<Signature> signingParties) throws FlowException {
        for (Signature signature : signingParties) {
            TargetAccount targetAccount = subFlow(new FindTargetAccountFlow(getCurrentAuthRequest().getBlockchainAddress(), signature.getBlockchainAddress()));
            if (targetAccount == null) continue;

            targetAccounts.add(targetAccount);
        }
        request.setTargetAccounts(targetAccounts);

        // Fetch all participants keys
        List<PublicKey> targetParticipantKeys = targetAccounts.stream().map(f -> f.getTargetAcctAnonymousParty().getOwningKey()).collect(Collectors.toList());
        if (!targetParticipantKeys.isEmpty()) {
            allKeys.addAll(targetParticipantKeys);
        }
        request.setParticipantKeys(allKeys);
    }

    @Suspendable
    protected void collectSignatures(R request) throws FlowException {

    }

    @Suspendable
    protected SignedTransaction notarise(R request) throws FlowException {
        return subFlow(new FinalityFlow(request.getSignedTransaction(), request.getTargetSessions()));
    }

    @Suspendable
    protected void createOtherOutputStates(R request) {
        // Nothing to be done by default
    }

    @Suspendable
    protected void createCurrentOutputState(R request) throws FlowException {
        // Nothing to be done by default
    }

    @Suspendable
    protected void createOtherInputStates(R request) throws FlowException {
        // Nothing to be done by default
    }

    protected void fetchCurrentInputState(R request) {
        if (this.request.getLinearId() != null) {
            request.setCurrentInputState(getStateQueryService().getByLinearId(getImplementingClass(), this.request.getLinearId()));
        }
    }

    protected abstract Class<T> getImplementingClass();

    @Suspendable
    protected void resolveSigningParties(R request) throws FlowException {
        if (!request.getInitialPartyList().isEmpty()) {
            request.setSigningParties(resolvePartiesFromString(request.getInitialPartyList()));
        }
        request.setSigningParties(subFlow(new UpdateSignaturesFlow<>(request)));
    }

    @Suspendable
    protected boolean onBeforeStart(R request) throws FlowException {
        return true;
    }

    @Suspendable
    protected void signInitialTransaction(R request, TransactionBuilder transactionBuilder) {
        request.setSignedTransaction(
                getServiceHub().signInitialTransaction(transactionBuilder)
        );
    }

    @Suspendable
    protected void resolveParticipants(R request) {
        if (request.getParticipantKeys().isEmpty()) {
            request.setParticipants(Collections.singletonList(getOurIdentity()));
            return;
        }
        request.setParticipants(request.getParticipantKeys().stream().map(AnonymousParty::new).collect(Collectors.toList()));
    }

    @Suspendable
    protected  TransactionBuilder buildTransaction(R request) throws FlowException {
        TransactionBuilder txBuilder = new TransactionBuilder(getNotary());
        if (request.getCurrentInputState() != null) {
            txBuilder.addInputState(request.getCurrentInputState());
        }
        request.getOtherInputStates().forEach(txBuilder::addInputState);

        addOutputState(request, txBuilder, request.getCurrentOutputState());

        for (StateCommandPair stateCommandPair : request.getOtherOutputStates()) {
            addOutputState(request, txBuilder, stateCommandPair);
        }

        if (request.getCurrentOutputState().getState() instanceof AbstractSmartState) {
            AbstractSmartState outputState = (AbstractSmartState) request.getCurrentOutputState().getState();

            for (FileMetadata attachment : outputState.getHeader().getAttachmentHashes()) {
                SecureHash hash = SecureHash.parse(attachment.getHash());
                txBuilder.addAttachment(hash);
            }
        }

        onBuildTransaction(txBuilder);

        return txBuilder;
    }

    @Suspendable
    private void addOutputState(R request, TransactionBuilder txBuilder, StateCommandPair stateCommandPair) {
        if (stateCommandPair.getState() != null) {
            txBuilder.addOutputState(stateCommandPair.getState());
        }

        List<PublicKey> keys;
        if (!request.getParticipantKeys().isEmpty()) {
            keys = request.getParticipantKeys();
        } else {
            keys = stateCommandPair
                    .getState()
                    .getParticipants()
                    .stream()
                    .map(AbstractParty::getOwningKey)
                    .collect(Collectors.toList());
        }
        txBuilder.addCommand(stateCommandPair.getCommandData(), keys);
    }

    @Suspendable
    protected void onBuildTransaction(TransactionBuilder txBuilder) {
        // By default there is nothing we can do
    }

    protected Party getNotary() throws FlowException {
        List<Party> notaries = getServiceHub().getNetworkMapCache().getNotaryIdentities();
        if (notaries.isEmpty()) {
            throw new FlowException("No notaries define. Please, contact network administrator.");
        }
        return notaries.get(0);
    }

    protected StateQueryService getStateQueryService() {
        if (this.stateQueryService == null) {
            this.stateQueryService = getServiceHub().cordaService(StateQueryService.class);
        }
        return this.stateQueryService;
    }

    protected BlockchainService getBlockchainService() {
        if (this.blockchainService == null) {
            this.blockchainService = getServiceHub().cordaService(BlockchainService.class);
        }
        return this.blockchainService;
    }

    private boolean allPartiesSigned(List<Signature> signingParties) {
        for (Signature signature : signingParties) {
            if (signature.isSigningParty() && (signature.getSigned() == null || Boolean.FALSE.equals(signature.getSigned()))) {
                return false;
            }
        }
        return true;
    }

    private Optional<Signature> getMySignature(List<Signature> signatures) {
        DecodedAuthRequest currentAuthRequest = getCurrentAuthRequest();
        if (currentAuthRequest == null) {
            return Optional.empty();
        }
        String me = currentAuthRequest.getBlockchainAddress().format();

        return signatures.stream().filter(p -> p.getBlockchainAddress().equals(me)).findFirst();
    }

    protected DecodedAuthRequest getCurrentAuthRequest() {
        return null;
    }

    @Suspendable
    protected List<Signature> resolvePartiesFromString(List<StringPair> parties) {
        List<Signature> result = new ArrayList<>();

        for (StringPair party : parties) {
            result.add(createSingleSignature(party));
        }
        return result;
    }

    @NotNull
    @Suspendable
    private Signature createSingleSignature(StringPair party) {
        Signature signature = new Signature();
        signature.setBlockchainAddress(party.getKey());
        signature.setSigningParty(Boolean.parseBoolean(party.getValue()));

        return signature;
    }
}
