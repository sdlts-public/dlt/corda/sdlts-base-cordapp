package io.sdlts.common.workflows;

import co.paralleluniverse.fibers.Suspendable;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.BlockchainAddress;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.workflows.internal.GetPartyInformationFlow;
import io.sdlts.common.workflows.internal.RequestPartyInformationFlow;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.*;
import net.corda.core.identity.CordaX500Name;

import java.util.Optional;

@InitiatingFlow
@StartableByRPC
@StartableByService
public class PartyInformationAddressFlow extends FlowLogic<Optional<PartyInformation>> {

    private final BlockchainAddress blockchainAddress;

    public PartyInformationAddressFlow(BlockchainAddress blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    @Suspendable
    @Override
    public Optional<PartyInformation> call() throws FlowException {
        PartyInformation pi = null;
        BlockchainAddress resolvedBlockchainAddress = resolvePartyForAddress(this.blockchainAddress);
        if (getOurIdentity().getName().equals(resolvedBlockchainAddress.getHost().getName())) {
            Optional<StateAndRef<PartyInformationState>> addressState = subFlow(new GetPartyInformationFlow(resolvedBlockchainAddress));
            if (addressState.isPresent()) {
                pi = addressState.get().getState().getData().getContent();
            }
        } else {
            PartyInformationState partyInformationState = subFlow(new RequestPartyInformationFlow(resolvedBlockchainAddress));

            pi = partyInformationState != null ? partyInformationState.getContent() : null;
        }
        return Optional.ofNullable(pi);
    }

    private BlockchainAddress resolvePartyForAddress(BlockchainAddress ba) {
        return BlockchainAddress.of(ba, getServiceHub().getIdentityService().wellKnownPartyFromX500Name(CordaX500Name.parse(ba.getHostAddress())));
    }
}
