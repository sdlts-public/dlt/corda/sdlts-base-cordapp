package io.sdlts.common;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public final class DefaultRoles {

    public static final String ADMIN = "admin";

    public static final String READER = "reader";

    private DefaultRoles() {}
}
