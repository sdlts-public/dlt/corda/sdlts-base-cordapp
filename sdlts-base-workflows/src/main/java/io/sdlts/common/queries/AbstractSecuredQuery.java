package io.sdlts.common.queries;

import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.model.UserDetails;
import io.sdlts.common.services.AuthDecoder;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.node.ServiceHub;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractSecuredQuery<T extends ContractState, R extends Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSecuredQuery.class);

    private final CordaRPCOps ops;

    private final ServiceHub serviceHub;

    protected final AuthRequest authRequest;

    protected AbstractSecuredQuery(AuthRequest authRequest, CordaRPCOps ops) {
        this(authRequest, ops, null);
    }

    protected AbstractSecuredQuery(AuthRequest authRequest, ServiceHub serviceHub) {
        this(authRequest, null, serviceHub);
    }

    protected AbstractSecuredQuery(AuthRequest authRequest, CordaRPCOps ops, ServiceHub serviceHub) {
        this.authRequest = authRequest;
        this.ops = ops;
        this.serviceHub = serviceHub;
    }

    public CordaRPCOps getOps() {
        return ops;
    }

    public ServiceHub getServiceHub() {
        return serviceHub;
    }

    public final Optional<StateAndRef<T>> firstState() {
        QueryCriteria heldByAccount = prepareBaseQuery();

        if (heldByAccount != null) {
            return queryStateAndRef(heldByAccount);
        }
        return Optional.empty();
    }

    private Optional<StateAndRef<T>> queryStateAndRef(QueryCriteria heldByAccount) {
        Vault.Page<T> result = queryStates(heldByAccount);
        if (result != null && !result.getStates().isEmpty()) {
            return Optional.of(result.getStates().get(0));
        }
        return Optional.empty();
    }

    public final Optional<R> first() {
        Optional<StateAndRef<T>> result = firstState();
        return result.map(preferencesStateStateAndRef -> transform(preferencesStateStateAndRef.getState().getData()));
    }

    public final List<R> list() {
        List<StateAndRef<T>> result = listStates();
        if (result.isEmpty()) {
            return new ArrayList<>();
        }
        return result.stream().map(i -> transform(i.getState().getData())).collect(Collectors.toList());
    }

    public final List<StateAndRef<T>> listStates() {
        QueryCriteria heldByAccount = prepareBaseQuery();

        if (heldByAccount != null) {
            Vault.Page<T> result =  queryStates(heldByAccount);
            if (result != null && !result.getStates().isEmpty()) {
                return result.getStates();
            }
        }
        return new ArrayList<>();
    }


    private QueryCriteria prepareBaseQuery() {
        if (this.authRequest == null) {
            return null;
        }
        UserDetails user = new AuthDecoder(this.authRequest).decode();

        List<SmartPermissionsState> permissions = new PermissionsQuery(this.ops, this.serviceHub, user.getId(), this.authRequest.getBlockchainAddress()).list();
        if (permissions.isEmpty()) {
            return null;
        }
        List<UUID> accountIdentifiers = permissions.stream().map(i -> i.getAccountIdentifier().getId()).collect(Collectors.toList());

        return new QueryCriteria.VaultQueryCriteria().withExternalIds(accountIdentifiers);
    }

    private Vault.Page<T> executeQuery(Class<T> stateClass, QueryCriteria queryCriteria) {
        if (this.ops != null) {
            return this.ops.vaultQueryByCriteria(queryCriteria, stateClass);
        }
        return this.serviceHub.getVaultService().queryBy(stateClass, queryCriteria);
    }

    private Vault.Page<T> executeQuery(Class<T> stateClass, QueryCriteria queryCriteria, PageSpecification paging, Sort sorting) {
        if (this.ops != null) {
            return this.ops.vaultQueryBy(queryCriteria, paging, sorting, stateClass);
        }
        return this.serviceHub.getVaultService().queryBy(stateClass, queryCriteria, paging, sorting);
    }

    private Vault.Page<T> queryStates(QueryCriteria heldByAccount) {
        final QueryProperties queryProperties = new QueryProperties();

        newQueryCriteria(queryProperties);

        if (queryProperties.isEmptyResult()) {
            return null;
        }

        QueryCriteria finalQuery = heldByAccount;
        if (queryProperties.isPresent()) {
            finalQuery = finalQuery.and(queryProperties.get());
        }

        if (queryProperties.hasPagingOrSorting()) {
            return executeQuery(getImplementingClass(), finalQuery, queryProperties.getPaging(), queryProperties.getSorting());
        }
        return executeQuery(getImplementingClass(), finalQuery);
    }

    protected void newQueryCriteria(QueryProperties queryProperties) {
        // By default - nothing to add
    }

    protected abstract Class<T> getImplementingClass();

    protected abstract R transform(T data);

    protected QueryCriteria createFieldCriteria(QueryCriteria externalIdQuery, String fieldName, Class<?> persistenceClass, Function<FieldInfo, QueryCriteria> fn) {
        try {
            FieldInfo fieldInfo = QueryCriteriaUtils.getField(fieldName, persistenceClass);
            QueryCriteria newCriteria = fn.apply(fieldInfo);
            if (externalIdQuery == null) {
                externalIdQuery = newCriteria;
            } else {
                externalIdQuery = externalIdQuery.and(newCriteria);
            }
            return externalIdQuery;
        } catch (NoSuchFieldException e) {
            LOGGER.error("No such field found: {}", fieldName, e);
        }
        return externalIdQuery;
    }

    @NotNull
    protected QueryCriteria createFieldCriteria(QueryCriteria externalIdQuery, String fieldName, Object fieldValue, Class<?> persistenceClass) {
        return createFieldCriteria(externalIdQuery, fieldName, persistenceClass, fieldInfo -> new QueryCriteria.VaultCustomQueryCriteria(Builder.equal(fieldInfo, fieldValue)));
    }

    protected QueryCriteria createDefaultCriteria(Class<?> persistenceClass) {
        return this.createFieldCriteria(null, "linearId", persistenceClass, fieldInfo -> new QueryCriteria.VaultCustomQueryCriteria(Builder.notNull(fieldInfo)));
    }

}
