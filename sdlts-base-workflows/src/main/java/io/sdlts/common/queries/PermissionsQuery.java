package io.sdlts.common.queries;

import com.google.common.base.Strings;
import io.sdlts.common.contracts.persistence.PermissionsSchema;
import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.DecodedAuthRequest;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.node.ServiceHub;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.Builder;
import net.corda.core.node.services.vault.FieldInfo;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.QueryCriteriaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PermissionsQuery extends AbstractQuery<SmartPermissionsState, SmartPermissionsState> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PermissionsQuery.class);

    private final String username;

    private final String blockChainAddress;

    public PermissionsQuery(ServiceHub serviceHub, String username) {
        this(null, serviceHub, username, null);
    }

    public PermissionsQuery(ServiceHub serviceHub, String username, String blockChainAddress) {
        this(null, serviceHub, username, blockChainAddress);
    }

    public PermissionsQuery(CordaRPCOps proxy, ServiceHub serviceHub, String username, String blockChainAddress) {
        super(proxy, serviceHub);
        this.username = username;
        this.blockChainAddress = blockChainAddress;
    }

    @Override
    protected SmartPermissionsState transform(SmartPermissionsState data) {
        return data;
    }

    @Override
    protected Vault.Page<SmartPermissionsState> queryStates(DecodedAuthRequest authRequest, QueryCriteria heldByAccount) {
        try {
            FieldInfo usernameField = QueryCriteriaUtils.getField("username", PermissionsSchema.SmartPermissionsPersistence.class);
            QueryCriteria customQueryCriteria = new QueryCriteria.VaultCustomQueryCriteria(Builder.equal(usernameField, this.username));

            if (!Strings.isNullOrEmpty(this.blockChainAddress)) {
                FieldInfo blockchainAddressField = QueryCriteriaUtils.getField("blockchainAddress", PermissionsSchema.SmartPermissionsPersistence.class);
                QueryCriteria blockchainAddressCriteria = new QueryCriteria.VaultCustomQueryCriteria(Builder.equal(blockchainAddressField, this.blockChainAddress));
                customQueryCriteria = customQueryCriteria.and(blockchainAddressCriteria);
            }
            return executeQuery(SmartPermissionsState.class, customQueryCriteria);
        } catch (NoSuchFieldException e) {
            LOGGER.error("Field was not found", e);
        }
        return null;
    }

    @Override
    protected Class<SmartPermissionsState> getImplementingClass() {
        return SmartPermissionsState.class;
    }
}
