package io.sdlts.common.queries;

import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.mapper.AuthRequest;
import net.corda.core.messaging.CordaRPCOps;

public class MyAccountsQuery extends AbstractSecuredQuery<SmartPermissionsState, SmartPermissionsState> {
    public MyAccountsQuery(AuthRequest authRequest, CordaRPCOps ops) {
        super(authRequest, ops);
    }

    @Override
    protected Class<SmartPermissionsState> getImplementingClass() {
        return SmartPermissionsState.class;
    }

    @Override
    protected SmartPermissionsState transform(SmartPermissionsState data) {
        return data;
    }
}
