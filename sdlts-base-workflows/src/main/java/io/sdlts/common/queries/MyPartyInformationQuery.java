package io.sdlts.common.queries;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import io.sdlts.common.contracts.persistence.PartyInformationSchema;
import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.mapper.AuthRequest;
import io.sdlts.common.model.PartyInformation;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.node.ServiceHub;
import net.corda.core.node.services.vault.PageSpecification;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.Sort;
import net.corda.core.node.services.vault.SortAttribute;

import java.util.UUID;

public class MyPartyInformationQuery extends AbstractSecuredQuery<PartyInformationState, PartyInformation> {
    private final MyPartyInformationFilter filter;

    public MyPartyInformationQuery(AuthRequest authRequest, CordaRPCOps ops) {
        this(authRequest, ops, null);
    }

    public MyPartyInformationQuery(AuthRequest authRequest, CordaRPCOps ops, MyPartyInformationFilter filter) {
        super(authRequest, ops);
        this.filter = filter;
    }

    public MyPartyInformationQuery(AuthRequest authRequest, ServiceHub serviceHub) {
        this(authRequest, serviceHub, null);
    }

    public MyPartyInformationQuery(AuthRequest authRequest, ServiceHub serviceHub, MyPartyInformationFilter filter) {
        super(authRequest, serviceHub);
        this.filter = filter;
    }

    @Override
    protected Class<PartyInformationState> getImplementingClass() {
        return PartyInformationState.class;
    }

    @Override
    protected PartyInformation transform(PartyInformationState data) {
        return data.getContent();
    }

    @Override
    protected void newQueryCriteria(QueryProperties queryProperties) {
        if (Strings.isNullOrEmpty(authRequest.getBlockchainAddress()) && !hasAtLeastOneFilterOption()) {
            return;
        }
        QueryCriteria customQueryCriteria = createDefaultCriteria(PartyInformationSchema.PartyInformationPersistence.class);

        if (!Strings.isNullOrEmpty(authRequest.getBlockchainAddress())) {
            customQueryCriteria = createFieldCriteria(customQueryCriteria, "blockchainAddress", authRequest.getBlockchainAddress(), PartyInformationSchema.PartyInformationPersistence.class);
        }

        if (!Strings.isNullOrEmpty(this.filter.getUniqueIdentifier())) {
            UUID contractUUID = UUID.fromString(this.filter.getUniqueIdentifier());
            customQueryCriteria = createFieldCriteria(customQueryCriteria, "linearId", contractUUID, PartyInformationSchema.PartyInformationPersistence.class);
        }

        if (!Strings.isNullOrEmpty(this.filter.getName())) {
            customQueryCriteria = createFieldCriteria(customQueryCriteria, "name", this.filter.getName(), PartyInformationSchema.PartyInformationPersistence.class);
        }
        if (!Strings.isNullOrEmpty(this.filter.getCompanyIdNumber())) {
            customQueryCriteria = createFieldCriteria(customQueryCriteria, "companyIdNumber", this.filter.getCompanyIdNumber(), PartyInformationSchema.PartyInformationPersistence.class);
        }
        if (!Strings.isNullOrEmpty(this.filter.getVat())) {
            customQueryCriteria = createFieldCriteria(customQueryCriteria, "vat", this.filter.getVat(), PartyInformationSchema.PartyInformationPersistence.class);
        }
        if (!Strings.isNullOrEmpty(this.filter.getCountryCode())) {
            customQueryCriteria = createFieldCriteria(customQueryCriteria, "countryCode", this.filter.getCountryCode().toLowerCase(), PartyInformationSchema.PartyInformationPersistence.class);
        }
        if (customQueryCriteria == null) {
            return;
        }
        Sort.SortColumn nameSortColumn = new Sort.SortColumn(new SortAttribute.Custom(PartyInformationSchema.PartyInformationPersistence.class, "name"), Sort.Direction.ASC);

        Sort sorting = new Sort(ImmutableList.of(nameSortColumn));

        PageSpecification paging = new PageSpecification(1, 50);

        queryProperties.setPaging(paging);
        queryProperties.setSorting(sorting);

        queryProperties.setQueryCriteria(customQueryCriteria);
    }

    private boolean hasAtLeastOneFilterOption() {
        if (this.filter == null) {
            return false;
        }
        return !Strings.isNullOrEmpty(this.filter.getUniqueIdentifier())
                || !Strings.isNullOrEmpty(this.filter.getName())
                || !Strings.isNullOrEmpty(this.filter.getVat())
                || !Strings.isNullOrEmpty(this.filter.getCountryCode())
                || !Strings.isNullOrEmpty(this.filter.getCompanyIdNumber());
    }
}
