package io.sdlts.common.queries;

import net.corda.core.node.services.vault.PageSpecification;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.Sort;

public class QueryProperties {
    private boolean emptyResult;

    private QueryCriteria queryCriteria;

    private PageSpecification paging;

    private Sort sorting;

    public Sort getSorting() {
        return sorting;
    }

    public void setSorting(Sort sorting) {
        this.sorting = sorting;
    }

    public PageSpecification getPaging() {
        return paging;
    }

    public void setPaging(PageSpecification paging) {
        this.paging = paging;
    }

    public boolean isEmptyResult() {
        return emptyResult;
    }

    public void setEmptyResult(boolean emptyResult) {
        this.emptyResult = emptyResult;
    }

    public boolean isPresent() {
        return this.queryCriteria != null;
    }

    public void setQueryCriteria(QueryCriteria queryCriteria) {
        this.queryCriteria = queryCriteria;
    }

    public QueryCriteria get() {
        return this.queryCriteria;
    }

    public boolean hasPagingOrSorting() {
        return this.paging != null || this.sorting != null;
    }
}
