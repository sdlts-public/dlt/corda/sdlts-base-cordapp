package io.sdlts.common.queries;

import io.sdlts.common.mapper.DecodedAuthRequest;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.node.ServiceHub;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.Builder;
import net.corda.core.node.services.vault.FieldInfo;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.QueryCriteriaUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractQuery<T extends ContractState, R extends Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractQuery.class);

    private final CordaRPCOps ops;

    private final ServiceHub serviceHub;

    protected AbstractQuery(CordaRPCOps ops) {
        this(ops, null);
    }

    protected AbstractQuery(ServiceHub serviceHub) {
        this(null, serviceHub);
    }

    protected AbstractQuery(CordaRPCOps ops, ServiceHub serviceHub) {
        this.ops = ops;
        this.serviceHub = serviceHub;
    }

    public final Optional<StateAndRef<T>> firstState() {
        return queryStateAndRef(null, null);
    }

    public final Optional<R> first() {
        Optional<StateAndRef<T>> result = firstState();
        return result.map(preferencesStateStateAndRef -> transform(preferencesStateStateAndRef.getState().getData()));
    }

    public final Optional<StateAndRef<T>> firstState(DecodedAuthRequest authRequest) {
        QueryCriteria heldByAccount = prepareBaseQuery(authRequest);

        if (heldByAccount != null) {
            return queryStateAndRef(authRequest, heldByAccount);
        }
        return Optional.empty();
    }

    private Optional<StateAndRef<T>> queryStateAndRef(DecodedAuthRequest authRequest, QueryCriteria heldByAccount) {
        Vault.Page<T> result = queryStates(authRequest, heldByAccount);
        if (result != null && !result.getStates().isEmpty()) {
            return Optional.of(result.getStates().get(0));
        }
        return Optional.empty();
    }

    public final Optional<R> first(DecodedAuthRequest authRequest) {
        Optional<StateAndRef<T>> result = firstState(authRequest);
        return result.map(preferencesStateStateAndRef -> transform(preferencesStateStateAndRef.getState().getData()));
    }

    public final List<R> list() {
        List<StateAndRef<T>> result = listStates();
        if (result.isEmpty()) {
            return new ArrayList<>();
        }
        return result.stream().map(i -> transform(i.getState().getData())).collect(Collectors.toList());
    }

    public final List<StateAndRef<T>> listStates() {
        Vault.Page<T> result = queryStates(null, null);
        if (result != null && !result.getStates().isEmpty()) {
            return result.getStates();
        }
        return new ArrayList<>();
    }


    private QueryCriteria prepareBaseQuery(DecodedAuthRequest authRequest) {
        if (authRequest == null || authRequest.getAccountIdentifier() == null) {
            return null;
        }
        return new QueryCriteria.VaultQueryCriteria().withExternalIds(Collections.singletonList(authRequest.getAccountIdentifier().getId()));
    }

    protected Vault.Page<T> executeQuery(Class<T> stateClass, QueryCriteria queryCriteria) {
        if (this.ops != null) {
            return this.ops.vaultQueryByCriteria(queryCriteria, stateClass);
        }
        return this.serviceHub.getVaultService().queryBy(stateClass, queryCriteria);
    }

    protected Vault.Page<T> queryStates(DecodedAuthRequest authRequest, QueryCriteria heldByAccount) {
        return executeQuery(getImplementingClass(), heldByAccount);
    }

    protected abstract Class<T> getImplementingClass();

    protected abstract R transform(T data);

    protected QueryCriteria createFieldCriteria(QueryCriteria externalIdQuery, String fieldName, Class<?> persistenceClass, Function<FieldInfo, QueryCriteria> fn) {
        try {
            FieldInfo fieldInfo = QueryCriteriaUtils.getField(fieldName, persistenceClass);
            QueryCriteria newCriteria = fn.apply(fieldInfo);
            if (externalIdQuery == null) {
                externalIdQuery = newCriteria;
            } else {
                externalIdQuery = externalIdQuery.and(newCriteria);
            }
            return externalIdQuery;
        } catch (NoSuchFieldException e) {
            LOGGER.error("No such field found: {}", fieldName, e);
        }
        return externalIdQuery;
    }

    @NotNull
    protected QueryCriteria createFieldCriteria(QueryCriteria externalIdQuery, String fieldName, Object fieldValue, Class<?> persistenceClass) {
        return createFieldCriteria(externalIdQuery, fieldName, persistenceClass, fieldInfo -> new QueryCriteria.VaultCustomQueryCriteria(Builder.equal(fieldInfo, fieldValue)));
    }

}
