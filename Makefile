publish:
	./gradlew publish -x test

tag_patch:
	semtag final -p -s patch

tag_minor:
	semtag final -p -s minor

tag_major:
	semtag final -p -s major

patch: tag_patch publish

minor: tag_minor publish

major: tag_major publish
