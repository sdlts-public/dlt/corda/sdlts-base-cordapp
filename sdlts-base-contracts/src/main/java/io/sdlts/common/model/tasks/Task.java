package io.sdlts.common.model.tasks;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public class Task {
    private Long id;

    private String name;

    private boolean billable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBillable() {
        return billable;
    }

    public void setBillable(boolean billable) {
        this.billable = billable;
    }
}
