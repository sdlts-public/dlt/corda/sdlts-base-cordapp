package io.sdlts.common.model;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public class GenericPair<K, V> {
    private K key;

    private V value;

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
