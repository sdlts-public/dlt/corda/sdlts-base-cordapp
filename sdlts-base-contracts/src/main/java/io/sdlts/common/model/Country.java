package io.sdlts.common.model;

import com.google.common.base.Strings;
import io.sdlts.common.mapper.CountryResolver;
import net.corda.core.serialization.CordaSerializable;

import java.util.*;

@CordaSerializable
public class Country {
    private String code;

    private String name;

    private List<StringPair> properties = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = !Strings.isNullOrEmpty(code) ? code.trim().toLowerCase() : code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StringPair> getProperties() {
        return properties;
    }

    public void setProperties(List<StringPair> properties) {
        this.properties = properties;
    }

    public static Country of(String code) {
        Locale locale = new Locale("en", code);
        return of(locale);
    }

    private static Country of(String code, String name, List<StringPair> properties) {
        Country result = new Country();
        result.code = code;
        result.name = name;
        result.properties = properties;
        return result;
    }

    public static Country of(Locale locale) {
        if  (locale == null){
            throw new IllegalStateException("Missing argument!");
        }
        return of(locale.getCountry(), locale.getDisplayCountry(), Collections.emptyList());
    }
    public static Country ofName(String name) {
        if (Strings.isNullOrEmpty(name)) {
            throw new IllegalStateException("Missing argument!");
        }
        Locale locale = CountryResolver.findByName(name);
        return of (locale);
    }

    public static boolean same(Country a, Country b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        return Objects.equals(a.getCode(), b.getCode());
    }

    public static boolean isValid(Country country) {
        return country != null && !Strings.isNullOrEmpty(country.getCode()) && !Strings.isNullOrEmpty(country.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(code, country.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
