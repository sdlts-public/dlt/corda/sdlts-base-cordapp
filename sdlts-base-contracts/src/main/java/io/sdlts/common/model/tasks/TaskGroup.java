package io.sdlts.common.model.tasks;

import net.corda.core.serialization.CordaSerializable;

import java.util.ArrayList;
import java.util.List;

@CordaSerializable
public class TaskGroup {
    private String name;

    private List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
