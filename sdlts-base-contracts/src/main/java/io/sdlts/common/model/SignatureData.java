package io.sdlts.common.model;

import net.corda.core.contracts.UniqueIdentifier;

public class SignatureData {
    private final UniqueIdentifier stateUniqueIdentifier;

    private final String stateClassType;

    public SignatureData(UniqueIdentifier stateUniqueIdentifier, String stateClassType) {
        this.stateUniqueIdentifier = stateUniqueIdentifier;
        this.stateClassType = stateClassType;
    }

    public UniqueIdentifier getStateUniqueIdentifier() {
        return stateUniqueIdentifier;
    }

    public String getStateClassType() {
        return stateClassType;
    }
}
