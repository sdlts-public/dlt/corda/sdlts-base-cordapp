package io.sdlts.common.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import net.corda.core.serialization.CordaSerializable;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@CordaSerializable
public class PartyInformation {
    private String name;

    private String additionalName;

    private String companyIdNumber;

    private String vat;

    private String blockchainAddress;

    private Address address;

    private String email;

    private String phone;

    private String comment;

    private List<StringPair> additionalInfo = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<StringPair> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(List<StringPair> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBlockchainAddress() {
        return blockchainAddress;
    }

    public void setBlockchainAddress(String blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    public String getCompanyIdNumber() {
        return companyIdNumber;
    }

    public void setCompanyIdNumber(String companyIdNumber) {
        this.companyIdNumber = companyIdNumber;
    }

    @Override
    public String toString() {
        return "PartyInformation{" +
                "name='" + name + '\'' +
                ", vat='" + vat + '\'' +
                ", address=" + address +
                '}';
    }
}
