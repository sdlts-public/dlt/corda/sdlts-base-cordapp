package io.sdlts.common.model;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public enum PartyInformationStatus {
    INCOMPLETE,
    ACTIVE,
    INACTIVE,
    READ;
}
