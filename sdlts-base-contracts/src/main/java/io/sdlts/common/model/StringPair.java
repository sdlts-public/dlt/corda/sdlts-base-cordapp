package io.sdlts.common.model;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public class StringPair extends GenericPair<String, String> {


    public static StringPair of(String key, String value) {
        StringPair pair = new StringPair();
        pair.setKey(key);
        pair.setValue(value);
        return pair;
    }
}
