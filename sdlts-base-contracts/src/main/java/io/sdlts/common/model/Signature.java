package io.sdlts.common.model;

import net.corda.core.serialization.CordaSerializable;

import java.util.Objects;

@CordaSerializable
public class Signature {
    private Boolean signed = Boolean.FALSE;

    private boolean signingParty = true;

    private String blockchainAddress;

    private String signatureDate;

    private UserDetails signedBy;

    private PartyInformation partyInformation;

    public boolean isSigningParty() {
        return signingParty;
    }

    public void setSigningParty(boolean signingParty) {
        this.signingParty = signingParty;
    }

    public Boolean getSigned() {
        return signed;
    }

    public void setSigned(Boolean signed) {
        this.signed = signed;
    }

    public String getBlockchainAddress() {
        return blockchainAddress;
    }

    public void setBlockchainAddress(String blockchainAddress) {
        this.blockchainAddress = blockchainAddress;
    }

    public String getSignatureDate() {
        return signatureDate;
    }

    public void setSignatureDate(String signatureDate) {
        this.signatureDate = signatureDate;
    }

    public UserDetails getSignedBy() {
        return signedBy;
    }

    public void setSignedBy(UserDetails signedBy) {
        this.signedBy = signedBy;
    }

    public PartyInformation getPartyInformation() {
        return partyInformation;
    }

    public void setPartyInformation(PartyInformation partyInformation) {
        this.partyInformation = partyInformation;
    }

    public Signature copy() {
        Signature result = new Signature();
        result.setPartyInformation(this.getPartyInformation());
        result.setBlockchainAddress(this.getBlockchainAddress());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Signature that = (Signature) o;
        return blockchainAddress.equals(that.blockchainAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blockchainAddress);
    }
}
