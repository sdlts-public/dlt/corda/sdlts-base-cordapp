package io.sdlts.common.model;

import net.corda.core.identity.AbstractParty;

import java.util.Date;
import java.util.List;

public class PartyInformationHeader extends Header {
    private final PartyInformationStatus status;

    public PartyInformationHeader(Date createDate,
                                  String creatorBlockchainAddress,
                                  List<Signature> signingParties,
                                  List<AbstractParty> participants,
                                  List<FileMetadata> attachmentHashes,
                                  PartyInformationStatus status) {
        super(createDate, creatorBlockchainAddress, signingParties, participants, attachmentHashes);
        this.status = status;
    }

    public PartyInformationStatus getStatus() {
        return status;
    }

    public static class Builder extends HeaderBuilder<PartyInformationHeader, Builder> {
        private PartyInformationStatus status;

        public Builder status(PartyInformationStatus status) {
            this.status = status;
            return this;
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public PartyInformationHeader build() {
            return new PartyInformationHeader(this.createDate,
                    this.creatorBlockchainAddress,
                    this.signingParties,
                    this.participants,
                    this.attachmentHashes,
                    this.status);
        }
    }
}
