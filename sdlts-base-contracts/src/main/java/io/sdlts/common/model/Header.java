package io.sdlts.common.model;

import net.corda.core.identity.AbstractParty;
import net.corda.core.serialization.ConstructorForDeserialization;
import net.corda.core.serialization.CordaSerializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CordaSerializable
public class Header {
    private final Date createDate;

    private final String creatorBlockchainAddress;

    private final List<Signature> signingParties;

    private final List<AbstractParty> participants;

    private final List<FileMetadata> attachmentHashes;

    private Header() {
        this(null, null, null, null, null);
    }

    @ConstructorForDeserialization
    public Header(Date createDate, String creatorBlockchainAddress, List<Signature> signingParties, List<AbstractParty> participants, List<FileMetadata> attachmentHashes) {
        this.createDate = createDate;
        this.creatorBlockchainAddress = creatorBlockchainAddress;
        this.signingParties = signingParties;
        this.participants = participants;
        this.attachmentHashes = attachmentHashes;
    }

    public String getCreatorBlockchainAddress() {
        return creatorBlockchainAddress;
    }

    public List<Signature> getSigningParties() {
        return signingParties;
    }

    public List<AbstractParty> getParticipants() {
        return participants;
    }

    public List<FileMetadata> getAttachmentHashes() {
        return attachmentHashes;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public abstract static class HeaderBuilder<R extends Header, B extends HeaderBuilder<R, B>> {
        protected String creatorBlockchainAddress;

        protected Date createDate;

        protected List<Signature> signingParties = new ArrayList<>();

        protected List<AbstractParty> participants = new ArrayList<>();

        protected List<FileMetadata> attachmentHashes = new ArrayList<>();

        public B createDate(Date createDate) {
            this.createDate = createDate;
            return self();
        }

        public B creatorBlockchainAddress(String creatorBlockchainAddress) {
            this.creatorBlockchainAddress = creatorBlockchainAddress;
            return self();
        }

        public B signingParties(List<Signature> signingParties) {
            this.signingParties = signingParties;
            return self();
        }

        public B participants(List<AbstractParty> participants) {
            this.participants = participants;
            return self();
        }

        public B attachmentHashes(List<FileMetadata> attachmentHashes) {
            this.attachmentHashes = attachmentHashes;
            return self();
        }

        protected abstract B self();

        public abstract R build();
    }
}
