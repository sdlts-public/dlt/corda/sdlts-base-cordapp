package io.sdlts.common.model;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public class Address {
    private String street;

    private String street2;

    private String zip;

    private String city;

    private Country country;

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public static class Builder extends Address {
        private final Address address = new Address();

        public Builder street(String street) {
            address.setStreet(street);
            return this;
        }

        public Builder zip(String zip) {
            address.setZip(zip);
            return this;
        }

        public Builder city(String city) {
            address.setCity(city);
            return this;
        }

        public Builder country(Country country) {
            address.setCountry(country);
            return this;
        }

        public Address build() {
            return this.address;
        }
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
