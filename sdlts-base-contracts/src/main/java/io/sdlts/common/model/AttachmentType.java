package io.sdlts.common.model;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public final class AttachmentType {
    public static final String CONTRACT = "CONTRACT";

    public static final String TIMESHEET = "TIMESHEET";

    public static final String INVOICE = "INVOICE";

    public static final String EMAIL = "EMAIL";

    public static final String OTHER = "OTHER";

    private AttachmentType() {}
}
