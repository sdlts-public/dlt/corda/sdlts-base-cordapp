package io.sdlts.common.mapper;

import io.sdlts.common.model.FileMetadata;
import io.sdlts.common.model.Signature;
import io.sdlts.common.model.StringPair;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.FlowSession;
import net.corda.core.identity.AbstractParty;
import net.corda.core.serialization.CordaSerializable;
import net.corda.core.transactions.SignedTransaction;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

@CordaSerializable
public abstract class TransactionRequest<C, T extends ContractState, R extends TransactionRequest<C, T, R>> {
    private UniqueIdentifier linearId;

    private C content;

    private List<FileMetadata> attachments = new ArrayList<>();

    private List<StringPair> initialPartyList = new ArrayList<>();

    private PublicKey myAccountPublicKey;

    private StateAndRef<T> currentInputState;

    private List<StateAndRef<? extends ContractState>> otherInputStates = new ArrayList<>();

    private List<TargetAccount> targetAccounts = new ArrayList<>();

    private boolean includeSigningParties;

    private List<Signature> signingParties = new ArrayList<>();

    private List<AbstractParty> participants = new ArrayList<>();

    private List<PublicKey> participantKeys = new ArrayList<>();

    private boolean allPartiesSigned;

    private StateCommandPair currentOutputState;

    private List<StateCommandPair> otherOutputStates = new ArrayList<>();

    private SignedTransaction signedTransaction;

    private List<FlowSession> targetSessions = new ArrayList<>();

    public StateAndRef<T> getCurrentInputState() {
        return currentInputState;
    }

    public void setCurrentInputState(StateAndRef<T> currentInputState) {
        this.currentInputState = currentInputState;
    }

    public List<StateAndRef<? extends ContractState>> getOtherInputStates() {
        return otherInputStates;
    }

    public void setOtherInputStates(List<StateAndRef<? extends ContractState>> otherInputStates) {
        this.otherInputStates = otherInputStates;
    }

    public List<AbstractParty> getParticipants() {
        return participants;
    }

    public void setParticipants(List<AbstractParty> participants) {
        this.participants = participants;
    }

    public StateCommandPair getCurrentOutputState() {
        return currentOutputState;
    }

    public void setCurrentOutputState(StateCommandPair currentOutputState) {
        this.currentOutputState = currentOutputState;
    }

    public List<StateCommandPair> getOtherOutputStates() {
        return otherOutputStates;
    }

    public void setOtherOutputStates(List<StateCommandPair> otherOutputStates) {
        this.otherOutputStates = otherOutputStates;
    }

    public SignedTransaction getSignedTransaction() {
        return signedTransaction;
    }

    public void setSignedTransaction(SignedTransaction signedTransaction) {
        this.signedTransaction = signedTransaction;
    }

    public List<FlowSession> getTargetSessions() {
        return targetSessions;
    }

    public void setTargetSessions(List<FlowSession> targetSessions) {
        this.targetSessions = targetSessions;
    }

    public List<TargetAccount> getTargetAccounts() {
        return targetAccounts;
    }

    public void setTargetAccounts(List<TargetAccount> targetAccounts) {
        this.targetAccounts = targetAccounts;
    }

    public PublicKey getMyAccountPublicKey() {
        return myAccountPublicKey;
    }

    public void setMyAccountPublicKey(PublicKey myAccountPublicKey) {
        this.myAccountPublicKey = myAccountPublicKey;
    }

    public List<PublicKey> getParticipantKeys() {
        return participantKeys;
    }

    public void setParticipantKeys(List<PublicKey> participantKeys) {
        this.participantKeys = participantKeys;
    }

    public boolean isAllPartiesSigned() {
        return allPartiesSigned;
    }

    public void setAllPartiesSigned(boolean allPartiesSigned) {
        this.allPartiesSigned = allPartiesSigned;
    }

    public boolean isIncludeSigningParties() {
        return includeSigningParties;
    }

    public void setIncludeSigningParties(boolean includeSigningParties) {
        this.includeSigningParties = includeSigningParties;
    }

    public List<Signature> getSigningParties() {
        return signingParties;
    }

    public void setSigningParties(List<Signature> signingParties) {
        this.signingParties = signingParties;
    }

    public C getContent() {
        return content;
    }

    public void setContent(C content) {
        this.content = content;
    }

    public List<StringPair> getInitialPartyList() {
        return initialPartyList;
    }

    public void setInitialPartyList(List<StringPair> initialPartyList) {
        this.initialPartyList = initialPartyList;
    }

    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    public void setLinearId(UniqueIdentifier linearId) {
        this.linearId = linearId;
    }

    public List<FileMetadata> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<FileMetadata> attachments) {
        this.attachments = attachments;
    }

    protected abstract R self();

    public R content(C content) {
        this.content = content;
        return self();
    }

    public R id(UniqueIdentifier linearId) {
        this.linearId = linearId;
        return self();
    }
}
