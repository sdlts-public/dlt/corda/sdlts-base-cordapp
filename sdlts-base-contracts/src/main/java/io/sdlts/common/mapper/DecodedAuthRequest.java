package io.sdlts.common.mapper;

import io.sdlts.common.contracts.states.SmartPermissionsState;
import io.sdlts.common.model.UserDetails;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.serialization.CordaSerializable;

import java.util.Optional;

@CordaSerializable
public class DecodedAuthRequest {
    private final AuthRequest authRequest;

    private final UserDetails user;

    private final BlockchainAddress blockchainAddress;

    private final UniqueIdentifier accountIdentifier;

    private DecodedAuthRequest(AuthRequest authRequest, UserDetails user, UniqueIdentifier accountIdentifier, BlockchainAddress blockchainAddress) {
        this.authRequest = authRequest;
        this.user = user;
        this.accountIdentifier = accountIdentifier;
        this.blockchainAddress = blockchainAddress;
    }

    public UserDetails getUser() {
        return user;
    }

    public UniqueIdentifier getAccountIdentifier() {
        return accountIdentifier;
    }

    public BlockchainAddress getBlockchainAddress() {
        return blockchainAddress;
    }

    public UserDetails toAuthenticatedUser() {
        return this.user;
    }

    public AuthRequest toAuthRequest() {
        return this.authRequest;
    }

    public static class Builder {
        private AuthRequest authRequest;

        private UserDetails user;

        private BlockchainAddress blockchainAddress;

        private UniqueIdentifier accountIdentifier;

        public Builder withAuthRequest(AuthRequest authRequest) {
            this.authRequest = authRequest;
            return this;
        }

        public Builder withBlockchainAddress(BlockchainAddress blockchainAddress) {
            this.blockchainAddress = blockchainAddress;
            return this;
        }

        public Builder withAccountIdentifier(Optional<SmartPermissionsState> smartPermissionsState) {
            smartPermissionsState.ifPresent(state -> this.accountIdentifier = state.getAccountIdentifier());
            return this;
        }

        public Builder withAccountIdentifier(UniqueIdentifier uniqueIdentifier) {
            this.accountIdentifier = uniqueIdentifier;
            return this;
        }

        public Builder withUser(UserDetails userDetails) {
            this.user = userDetails;
            return this;
        }

        public DecodedAuthRequest build() {
            return new DecodedAuthRequest(authRequest, user, accountIdentifier, blockchainAddress);
        }
    }
}
