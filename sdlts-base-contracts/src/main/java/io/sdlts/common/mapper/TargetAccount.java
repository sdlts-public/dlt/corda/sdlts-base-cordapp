package io.sdlts.common.mapper;

import com.r3.corda.lib.accounts.contracts.states.AccountInfo;
import net.corda.core.identity.AnonymousParty;

public class TargetAccount {
    private AccountInfo accountInfo;

    private AnonymousParty targetAcctAnonymousParty;

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public AnonymousParty getTargetAcctAnonymousParty() {
        return targetAcctAnonymousParty;
    }

    public void setTargetAcctAnonymousParty(AnonymousParty targetAcctAnonymousParty) {
        this.targetAcctAnonymousParty = targetAcctAnonymousParty;
    }
}
