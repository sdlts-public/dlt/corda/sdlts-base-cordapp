package io.sdlts.common.mapper;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.ContractState;

public class StateCommandPair {
    private ContractState state;

    private CommandData commandData;

    public StateCommandPair() {
    }

    public StateCommandPair(ContractState state, CommandData commandData) {
        this.state = state;
        this.commandData = commandData;
    }

    public ContractState getState() {
        return state;
    }

    public void setState(ContractState state) {
        this.state = state;
    }

    public CommandData getCommandData() {
        return commandData;
    }

    public void setCommandData(CommandData commandData) {
        this.commandData = commandData;
    }
}
