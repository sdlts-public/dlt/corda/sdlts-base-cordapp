package io.sdlts.common.mapper;

import com.google.common.base.Strings;
import io.sdlts.common.model.UserDetails;

public class UserHelper {
    private UserHelper() {}

    private static String resolve(String firstName, String lastName) {
        StringBuilder sb = new StringBuilder();
        if (!Strings.isNullOrEmpty(firstName)) {
            sb.append(firstName).append(' ');
        }
        if (!Strings.isNullOrEmpty(lastName)) {
            sb.append(lastName);
        }
        return sb.toString().trim();
    }

    public static UserDetails of(String id, String firstName, String lastName, String email) {
        String name = resolve(firstName, lastName);
        return new UserDetails(id, name, email);
    }
}
