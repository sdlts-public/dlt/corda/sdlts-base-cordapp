package io.sdlts.common.mapper;

import com.r3.corda.lib.accounts.contracts.states.AccountInfo;

public class AccountInfoTransactionRequest extends TransactionRequest<AccountInfo, AccountInfo, AccountInfoTransactionRequest> {
    @Override
    protected AccountInfoTransactionRequest self() {
        return this;
    }
}
