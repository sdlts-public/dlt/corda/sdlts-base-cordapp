package io.sdlts.common.mapper;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CountryResolver {
    private static final Map<String, Locale> reverseCountries = new HashMap<>();

    static {
        for (String iso : Locale.getISOCountries()) {
            Locale locale = new Locale("en", iso);
            reverseCountries.put(locale.getDisplayCountry().toLowerCase(), locale);
        }
    }
    private CountryResolver() {}

    public static Locale findByName(String name) {
        return reverseCountries.get(name.toLowerCase());
    }
}
