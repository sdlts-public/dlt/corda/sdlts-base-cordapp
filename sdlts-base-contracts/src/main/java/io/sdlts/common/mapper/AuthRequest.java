package io.sdlts.common.mapper;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public class AuthRequest {
    private final String token;

    private final String blockchainAddress;

    private final AuthType authType;

    public AuthRequest(String token, String blockchainAddress, AuthType authType) {
        this.token = token;
        this.blockchainAddress = blockchainAddress;
        this.authType = authType;
    }

    public static AuthRequest asSystemUser(BlockchainAddress creatorBlockchainAddress) {
        return withAddress("SYSTEM_USER_NAME", creatorBlockchainAddress);
    }

    public static AuthRequest copy(AuthRequest authRequest, String activeBlockchainAddress) {
        return of(authRequest.getToken(), activeBlockchainAddress, authRequest.getAuthType());
    }

    public AuthType getAuthType() {
        return authType;
    }

    public String getToken() {
        return token;
    }

    public String getBlockchainAddress() {
        return blockchainAddress;
    }

    public static AuthRequest of(String userIdentifier) {
        return of (userIdentifier, null);
    }

    public static AuthRequest withAddress(String userIdentifier, BlockchainAddress blockchainAddress) {
        return of(userIdentifier, blockchainAddress != null ? blockchainAddress.format() : null, AuthType.LDAP);
    }

    public static AuthRequest of(String userIdentifier, String blockchainAddress) {
        return of(userIdentifier, blockchainAddress, AuthType.LDAP);
    }

    public static AuthRequest of(String userIdentifier, String blockchainAddress, AuthType authType) {
        return new AuthRequest(userIdentifier, blockchainAddress, authType);
    }

    public AuthRequest withoutBlockchainAddress() {
        return of(this.token, null, this.authType);
    }
}
