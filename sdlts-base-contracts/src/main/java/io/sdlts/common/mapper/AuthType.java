package io.sdlts.common.mapper;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public enum AuthType {
    AUTH0,
    SHIRO,
    OAUTH2,
    LDAP,
    SERVICE_ACCOUNT,
    USERNAME
}
