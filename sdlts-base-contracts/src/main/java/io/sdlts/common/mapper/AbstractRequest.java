package io.sdlts.common.mapper;

import io.sdlts.common.model.Signature;
import net.corda.core.identity.AbstractParty;
import net.corda.core.serialization.ConstructorForDeserialization;
import net.corda.core.serialization.CordaSerializable;

import java.util.List;

@CordaSerializable
public class AbstractRequest<T> {

    private String creatorBlockchainAddress;

    private T content;

    private List<Signature> signingParties;

    private List<AbstractParty> participants;

    public AbstractRequest() {}

    @ConstructorForDeserialization
    public AbstractRequest(String creatorBlockchainAddress, T content, List<Signature> signingParties, List<AbstractParty> participants) {
        this.creatorBlockchainAddress = creatorBlockchainAddress;
        this.content = content;
        this.signingParties = signingParties;
        this.participants = participants;
    }

    public String getCreatorBlockchainAddress() {
        return creatorBlockchainAddress;
    }

    public void setCreatorBlockchainAddress(String creatorBlockchainAddress) {
        this.creatorBlockchainAddress = creatorBlockchainAddress;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public List<Signature> getSigningParties() {
        return signingParties;
    }

    public void setSigningParties(List<Signature> signingParties) {
        this.signingParties = signingParties;
    }

    public List<AbstractParty> getParticipants() {
        return participants;
    }

    public void setParticipants(List<AbstractParty> participants) {
        this.participants = participants;
    }

    public abstract static class AbstractBuilder<C, B extends AbstractBuilder<C, B, R>, R extends AbstractRequest<C>> {
        private final R request;

        protected AbstractBuilder(R instance) {
            this.request = instance;
        }

        public B creatorBlockchainAddress(String creatorBlockchainAddress) {
            this.request.setCreatorBlockchainAddress(creatorBlockchainAddress);
            return self();
        }

        public B content(C content) {
            this.request.setContent(content);
            return self();
        }

        public B signingParties(List<Signature> signingParties) {
            this.request.setSigningParties(signingParties);
            return self();
        }

        public B participants(List<AbstractParty> participants) {
            this.request.setParticipants(participants);
            return self();
        }

        protected R me() {
            return this.request;
        }
        protected abstract B self();

        public R build() {
            return this.request;
        }
    }
}
