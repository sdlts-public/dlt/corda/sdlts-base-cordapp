package io.sdlts.common.mapper;

import io.sdlts.common.contracts.states.PartyInformationState;
import io.sdlts.common.model.PartyInformation;

public class PartyInformationTransactionRequest extends TransactionRequest<PartyInformation, PartyInformationState, PartyInformationTransactionRequest> {

    @Override
    protected PartyInformationTransactionRequest self() {
        return this;
    }
}
