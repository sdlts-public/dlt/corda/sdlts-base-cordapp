package io.sdlts.common.mapper;

import com.google.common.base.Strings;
import net.corda.core.identity.Party;
import net.corda.core.serialization.CordaSerializable;
import org.apache.commons.lang3.StringUtils;

@CordaSerializable
public class BlockchainAddress {
    private final String accountNumber;

    private final String hostAddress;

    private final Party host;

    public BlockchainAddress(String accountNumber, String hostAddress, Party host) {
        this.accountNumber = accountNumber;
        this.hostAddress = hostAddress;
        this.host = host;
    }

    public static BlockchainAddress of(String accountNumber, Party host) {
        return of(accountNumber, host.getName().toString(), host);
    }

    public static BlockchainAddress of(BlockchainAddress temporary, Party host) {
        return of(temporary.getAccountNumber(), temporary.getHostAddress(), host);
    }

    public static BlockchainAddress of(String accountNumber, String hostAddress, Party host) {
        return new BlockchainAddress(accountNumber, hostAddress, host);
    }

    public static BlockchainAddress parse(String blockchainAddress) {
        if (blockchainAddress != null && blockchainAddress.startsWith("ACC=")) {
            String accountNumber = StringUtils.substringBetween(blockchainAddress, "ACC=", ",").trim();
            String hostAddress = blockchainAddress.substring(blockchainAddress.indexOf(",") + 1).trim();
            return of(accountNumber, hostAddress, null);
        }
        return null;
    }

    public static boolean isValid(String blockchainAddress) {
        if (Strings.isNullOrEmpty(blockchainAddress)) {
            return false;
        }
        if (!blockchainAddress.contains(",")) {
            return false;
        }
        String[] parts = blockchainAddress.split(",");
        if (parts.length < 4) {
            return false;
        }
        if (!parts[0].trim().startsWith("ACC=")) {
            return false;
        }
        if (!parts[1].trim().startsWith("O=")) {
            return false;
        }
        if (!parts[2].trim().startsWith("L=")) {
            return false;
        }
        return parts[3].trim().startsWith("C=");
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public Party getHost() {
        return host;
    }

    public String format() {
        if (Strings.isNullOrEmpty(this.hostAddress)) {
            return "NO_DATA";
        }
        if (Strings.isNullOrEmpty(this.accountNumber)) {
            return this.hostAddress;
        }
        return String.format("ACC=%s, %s", this.accountNumber, this.hostAddress);
    }

    @Override
    public String toString() {
        return format();
    }
}
