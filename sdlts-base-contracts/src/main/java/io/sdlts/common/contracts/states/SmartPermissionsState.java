package io.sdlts.common.contracts.states;

import com.google.common.collect.ImmutableList;
import io.sdlts.common.contracts.PermissionContract;
import io.sdlts.common.contracts.persistence.PermissionsSchema;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import net.corda.core.schemas.QueryableState;
import net.corda.core.serialization.ConstructorForDeserialization;
import net.corda.core.serialization.CordaSerializable;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@BelongsToContract(PermissionContract.class)
@CordaSerializable
public class SmartPermissionsState implements LinearState, QueryableState {
    private final UniqueIdentifier linearId;

    private final UniqueIdentifier accountIdentifier;

    private final String username;

    private final String partyAddress;

    private final String role;

    private final List<AbstractParty> participants;

    public SmartPermissionsState(UniqueIdentifier accountIdentifier, String username, String partyAddress, String role, List<AbstractParty> participants) {
        this (accountIdentifier, username, partyAddress, role, participants, new UniqueIdentifier());
    }

    @ConstructorForDeserialization
    public SmartPermissionsState(UniqueIdentifier accountIdentifier,
                                 String username,
                                 String partyAddress,
                                 String role,
                                 List<AbstractParty> participants,
                                 UniqueIdentifier linearId) {
        this.accountIdentifier = accountIdentifier;
        this.username = username;
        this.partyAddress = partyAddress;
        this.role = role;
        this.participants = participants;
        this.linearId = linearId;
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    public UniqueIdentifier getAccountIdentifier() {
        return accountIdentifier;
    }

    @NotNull
    @Override
    public Iterable<MappedSchema> supportedSchemas() {
        return ImmutableList.of(new PermissionsSchema());
    }

    @NotNull
    @Override
    public PersistentState generateMappedObject(@NotNull MappedSchema schema) {
        if (schema instanceof PermissionsSchema) {
            return new PermissionsSchema.SmartPermissionsPersistence(this.username, this.partyAddress, this.role, getLinearId().getId());
        } else {
            throw new IllegalArgumentException("Unrecognised schema $schema");
        }
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return this.participants;
    }

    public String getUsername() {
        return username;
    }

    public String getPartyAddress() {
        return partyAddress;
    }

    public String getRole() {
        return role;
    }
}

