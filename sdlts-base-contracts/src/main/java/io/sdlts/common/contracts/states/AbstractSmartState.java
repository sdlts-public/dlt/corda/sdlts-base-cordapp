package io.sdlts.common.contracts.states;

import io.sdlts.common.model.Header;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class AbstractSmartState<H extends Header, C> implements LinearState {
    private final UniqueIdentifier linearId;

    private final H header;

    private final C content;

    protected AbstractSmartState(H header, C content, UniqueIdentifier linearId) {
        this.header = header;
        this.content = content;
        this.linearId = linearId;
    }

    public H getHeader() {
        return header;
    }

    public C getContent() {
        return content;
    }

    @Override
    @NotNull
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        if (this.header == null) {
            throw  new IllegalStateException("Header is missing");
        }
        return this.header.getParticipants();
    }
}
