package io.sdlts.common.contracts.states;

import com.google.common.collect.ImmutableList;
import io.sdlts.common.contracts.SignatureContract;
import io.sdlts.common.contracts.persistence.SignatureSchema;
import io.sdlts.common.model.Header;
import io.sdlts.common.model.SignatureData;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import net.corda.core.schemas.QueryableState;
import net.corda.core.serialization.ConstructorForDeserialization;
import net.corda.core.serialization.CordaSerializable;
import org.jetbrains.annotations.NotNull;

@BelongsToContract(SignatureContract.class)
@CordaSerializable
public class SignatureState extends AbstractSmartState<Header, SignatureData> implements QueryableState {


    public SignatureState(Header header, SignatureData content) {
        this (header, content, new UniqueIdentifier());
    }

    @ConstructorForDeserialization
    public SignatureState(Header header, SignatureData content, UniqueIdentifier linearId) {
        super(header, content, linearId);
    }

    @NotNull
    @Override
    public PersistentState generateMappedObject(@NotNull MappedSchema schema) {
        if (schema instanceof SignatureSchema) {
            return new SignatureSchema.SmartSignaturePersistence(
                    getLinearId().getId(),
                    this.getContent().getStateUniqueIdentifier().getId(),
                    this.getContent().getStateClassType());
        } else {
            throw new IllegalArgumentException("Unrecognised schema $schema");
        }
    }

    @NotNull
    @Override
    public Iterable<MappedSchema> supportedSchemas() {
        return ImmutableList.of(new SignatureSchema());
    }

}

