package io.sdlts.common.contracts;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;
import org.jetbrains.annotations.NotNull;

public class PermissionContract implements Contract {
    public static final String ID = PermissionContract.class.getName();

    @Override
    public void verify(@NotNull LedgerTransaction tx) {
        //TODO implement verification
    }

    public interface Commands extends CommandData {
        class AssignAccountToUser implements Commands {
        }
    }
}
