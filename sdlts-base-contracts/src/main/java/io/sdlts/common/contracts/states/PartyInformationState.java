package io.sdlts.common.contracts.states;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import io.sdlts.common.contracts.PartyInformationContract;
import io.sdlts.common.contracts.persistence.PartyInformationSchema;
import io.sdlts.common.model.Address;
import io.sdlts.common.model.PartyInformation;
import io.sdlts.common.model.PartyInformationHeader;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import net.corda.core.schemas.QueryableState;
import net.corda.core.serialization.ConstructorForDeserialization;
import net.corda.core.serialization.CordaSerializable;
import org.jetbrains.annotations.NotNull;

@BelongsToContract(PartyInformationContract.class)
@CordaSerializable
public class PartyInformationState extends AbstractSmartState<PartyInformationHeader, PartyInformation> implements QueryableState {
    public PartyInformationState(PartyInformationHeader header, PartyInformation content) {
        this(header, content, new UniqueIdentifier());
    }

    @ConstructorForDeserialization
    public PartyInformationState(PartyInformationHeader header, PartyInformation content, UniqueIdentifier linearId) {
        super(header, content, linearId);
    }

    @NotNull
    @Override
    public PersistentState generateMappedObject(@NotNull MappedSchema schema) {
        if (schema instanceof PartyInformationSchema) {
            String countryCode = resolveCountryCode(this.getContent().getAddress());
            return new PartyInformationSchema.PartyInformationPersistence(
                    getLinearId().getId(),
                    this.getHeader().getCreatorBlockchainAddress(),
                    this.getContent().getName(),
                    this.getContent().getCompanyIdNumber(),
                    this.getContent().getVat(),
                    countryCode
            );
        } else {
            throw new IllegalArgumentException("Unrecognised schema $schema");
        }
    }

    private String resolveCountryCode(Address address) {
        if (address != null && address.getCountry() != null && !Strings.isNullOrEmpty(address.getCountry().getCode())) {
            return address.getCountry().getCode();
        }
        return null;
    }

    @NotNull
    @Override
    public Iterable<MappedSchema> supportedSchemas() {
        return ImmutableList.of(new PartyInformationSchema());
    }
}

