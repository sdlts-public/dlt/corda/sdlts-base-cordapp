package io.sdlts.common.contracts.persistence;

import com.google.common.collect.ImmutableList;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

public class SignatureSchema extends MappedSchema {
    public SignatureSchema() {
        super(SignatureSchema.class, 1, ImmutableList.of(SmartSignaturePersistence.class));
    }

    @Nullable
    @Override
    public String getMigrationResource() {
        return "smi-signature.changelog-master";
    }

    @Entity
    @Table(name = "smi_signature_state")
    public static class SmartSignaturePersistence extends PersistentState {
        @Column(name = "linear_id")
        private final UUID linearId;

        @Column(name = "signed_state_id")
        private final UUID stateUniqueIdentifier;

        @Column(name = "state_class_type")
        private final String stateClassType;

        // Default constructor required by hibernate.
        public SmartSignaturePersistence() {
            this(null, null, null);
        }

        public SmartSignaturePersistence(UUID linearId, UUID stateUniqueIdentifier, String stateClassType) {
            this.linearId = linearId;
            this.stateUniqueIdentifier = stateUniqueIdentifier;
            this.stateClassType = stateClassType;
        }

        public UUID getStateUniqueIdentifier() {
            return stateUniqueIdentifier;
        }

        public String getStateClassType() {
            return stateClassType;
        }

        public UUID getLinearId() {
            return linearId;
        }
    }
}
