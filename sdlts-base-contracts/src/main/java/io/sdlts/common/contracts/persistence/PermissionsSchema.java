package io.sdlts.common.contracts.persistence;

import com.google.common.collect.ImmutableList;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

public class PermissionsSchema extends MappedSchema {
    public PermissionsSchema() {
        super(PermissionsSchema.class, 1, ImmutableList.of(SmartPermissionsPersistence.class));
    }

    @Nullable
    @Override
    public String getMigrationResource() {
        return "smi-permissions.changelog-master";
    }

    @Entity
    @Table(name = "smi_permissions_state")
    public static class SmartPermissionsPersistence extends PersistentState {
        @Column(name = "linear_id")
        private final UUID linearId;

        @Column(name = "username_")
        private final String username;

        @Column(name = "blockchain_address")
        private final String blockchainAddress;

        @Column(name = "role_")
        private final String role;

        // Default constructor required by hibernate.
        public SmartPermissionsPersistence() {
            this(null, null, null, null);
        }

        public SmartPermissionsPersistence(String username, String blockchainAddress, String role, UUID linearId) {
            this.username = username;
            this.blockchainAddress = blockchainAddress;
            this.role = role;
            this.linearId = linearId;
        }

        public UUID getLinearId() {
            return linearId;
        }

        public String getUsername() {
            return username;
        }

        public String getBlockchainAddress() {
            return blockchainAddress;
        }
    }
}
