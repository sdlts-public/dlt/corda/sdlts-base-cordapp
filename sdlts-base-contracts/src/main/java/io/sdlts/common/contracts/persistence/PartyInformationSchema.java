package io.sdlts.common.contracts.persistence;

import com.google.common.collect.ImmutableList;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

public class PartyInformationSchema extends MappedSchema {
    public PartyInformationSchema() {
        super(PartyInformationSchema.class, 1, ImmutableList.of(PartyInformationPersistence.class));
    }

    @Nullable
    @Override
    public String getMigrationResource() {
        return "smi-address.changelog-master";
    }

    @Entity
    @Table(name = "smi_party_information_state")
    public static class PartyInformationPersistence extends PersistentState {
        @Column(name = "linear_id")
        private final UUID linearId;

        @Column(name = "blockchain_address")
        private final String blockchainAddress;

        @Column(name = "name_")
        private final String name;

        @Column(name = "company_id_number")
        private final String companyIdNumber;

        @Column(name = "vat_")
        private final String vat;

        @Column(name = "country_code")
        private final String countryCode;


        // Default constructor required by hibernate.
        public PartyInformationPersistence() {
            this(null, null, null, null, null, null);
        }

        public PartyInformationPersistence(UUID linearId, String blockchainAddress, String name, String companyIdNumber, String vat, String countryCode) {
            this.blockchainAddress = blockchainAddress;
            this.name = name;
            this.linearId = linearId;
            this.companyIdNumber = companyIdNumber;
            this.vat = vat;
            this.countryCode = countryCode;
        }

        public String getName() {
            return name;
        }

        public UUID getLinearId() {
            return linearId;
        }

        public String getBlockchainAddress() {
            return blockchainAddress;
        }

        public String getCompanyIdNumber() {
            return companyIdNumber;
        }

        public String getVat() {
            return vat;
        }

        public String getCountryCode() {
            return countryCode;
        }
    }
}
